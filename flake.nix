{
  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    rust-overlay.url = "github:oxalica/rust-overlay";
    naersk.url = "github:nix-community/naersk";
  };

  outputs = { self, nixpkgs, flake-utils, rust-overlay, naersk, ... }:
    flake-utils.lib.eachDefaultSystem (
      system: let
        # pkgs = nixpkgs.legacyPackages."${system}";
        # naersk-lib = naersk.lib."${system}";

        overlays = [ (import rust-overlay) ];
        pkgs = import nixpkgs { inherit system overlays; };
        myRust = pkgs.rust-bin.selectLatestNightlyWith (toolchain: toolchain.default);
        naersk-lib = naersk.lib."${system}".override {
          cargo = myRust;
          rustc = myRust;
        };
      in
        rec {
          # `nix build`
          defaultPackage = naersk-lib.buildPackage {
            pname = "tenso-rs";
            root = ./.;
            buildInputs = with pkgs; [
              pkg-config
              cmake
              openssl
            ];
          };

          # `nix run`
          apps.default = flake-utils.lib.mkApp {
            drv = self.defaultPackage."${system}";
          };

          # `nix develop`
          devShell = pkgs.mkShell {
            nativeBuildInputs = with pkgs; [ 
              pkg-config
              cmake
              openssl

              myRust
              rust-analyzer
              hotspot
            ];
          };
        }
    );
}
