use criterion::{black_box, criterion_group, criterion_main, Criterion};
use tenso_rs::tensor;

pub fn criterion_benchmark(c: &mut Criterion) {
    let data1 = black_box(
        tensor::Tensor::<i64>::arange(0, 150000000, 1)
            .unwrap()
            .reshape(vec![500, 1000, 300])
            .unwrap(),
    );
    let data2 = black_box(
        tensor::Tensor::<i64>::arange(150000000, 350000000, 1)
            .unwrap()
            .reshape(vec![500, 1000, 400])
            .unwrap(),
    );
    c.bench_function("cat 500x1000x300 + 500x1000x400", |b| {
        b.iter(|| data1.cat(&data2, 2))
    });
}

criterion_group! {
    name = benches;
    config = Criterion::default().sample_size(75);
    targets = criterion_benchmark
}
criterion_main!(benches);
