use std::{cell::RefCell, ops, rc::Rc};

use num::{Bounded, Float, NumCast, ToPrimitive};

use crate::{errors, types};

use super::{
    storage::{self, StorageTrait},
    utils, Tensor,
};

impl<T: Bounded + NumCast + ToPrimitive + Copy + PartialOrd + ops::AddAssign> Tensor<T> {
    pub fn from_array(vec: Vec<T>, dim: Vec<u64>) -> Result<Tensor<T>, errors::ErrorKind> {
        let exp_len: u64 = dim.iter().product();
        if vec.len() as u64 != exp_len {
            return Err(errors::ErrorKind::InvalidParameter(
                format!(
                    "Invalid size of vec, expected: {}, got: {}",
                    exp_len,
                    vec.len()
                )
                .to_string(),
            ));
        }

        let storage = Rc::new(RefCell::new(storage::Storage::new(vec)));
        let strides = utils::strides_from_dimensions(dim.clone());

        Ok(Tensor::<T>::from_raw(
            storage,
            dim,
            types::type_from_range(
                T::min_value().to_f64().unwrap(),
                T::max_value().to_f64().unwrap(),
            )
            .unwrap(),
            strides,
            0,
        ))
    }

    pub fn from_val(val: T, dim: Vec<u64>) -> Result<Tensor<T>, errors::ErrorKind> {
        let prod: u64 = dim.iter().product();
        let storage_val: Vec<T> = vec![val; prod as usize];
        Tensor::<T>::from_array(storage_val, dim)
    }

    pub fn zeros(dim: Vec<u64>) -> Result<Tensor<T>, errors::ErrorKind> {
        let zero = NumCast::from(0).unwrap();
        Tensor::from_val(zero, dim)
    }
    pub fn ones(dim: Vec<u64>) -> Result<Tensor<T>, errors::ErrorKind> {
        let one = NumCast::from(1).unwrap();
        Tensor::from_val(one, dim)
    }

    pub fn arange(start: T, end: T, step: T) -> Result<Tensor<T>, errors::ErrorKind> {
        if start >= end {
            return Err(errors::ErrorKind::InvalidParameter(
                "Expected Start < End".to_string(),
            ));
        }
        let mut storage_val = vec![];
        let mut curr = start;
        while curr < end {
            storage_val.push(curr);
            curr += step;
        }
        let len = storage_val.len();
        Tensor::<T>::from_array(storage_val, vec![len as u64])
    }

    pub fn eye(n: u64, m: u64) -> Result<Tensor<T>, errors::ErrorKind> {
        if n == 0 || m == 0 {
            return Err(errors::ErrorKind::InvalidParameter(
                "Expected N, M > 0".to_string(),
            ));
        }

        let zero: T = NumCast::from(0).unwrap();
        let one: T = NumCast::from(1).unwrap();
        let mut storage_val = vec![];
        for row in 0..n {
            for col in 0..m {
                storage_val.push(if col == row { one } else { zero });
            }
        }
        Tensor::<T>::from_array(storage_val, vec![n, m])
    }
}

impl<T: Float + Bounded + NumCast + ToPrimitive + Copy + PartialOrd + ops::AddAssign> Tensor<T> {
    // -------------------------- Creation Methods --------------------------
    pub fn linspace(start: T, end: T, n: u64) -> Result<Tensor<T>, errors::ErrorKind> {
        if start >= end {
            return Err(errors::ErrorKind::InvalidParameter(
                "Expected Start < End".to_string(),
            ));
        }

        if n == 0 {
            return Err(errors::ErrorKind::InvalidParameter(
                "Expected N > 0".to_string(),
            ));
        }

        let n_float: T = NumCast::from(n - 1).unwrap();
        let dx = (end - start) / n_float;
        let mut storage_val = vec![start; n as usize];
        for i in 1..(n as usize) {
            storage_val[i] = storage_val[i - 1] + NumCast::from(dx).unwrap();
        }

        let len = storage_val.len();
        let new_storage = Rc::new(RefCell::new(storage::Storage::new(storage_val)));

        Ok(Tensor::<T>::from_raw(
            new_storage,
            vec![len as u64],
            types::type_from_range(
                <T as Bounded>::min_value().to_f64().unwrap(),
                <T as Bounded>::max_value().to_f64().unwrap(),
            )
            .unwrap(),
            vec![1u64],
            0,
        ))
    }

    pub fn logspace(start: T, end: T, n: u64, base: T) -> Result<Tensor<T>, errors::ErrorKind> {
        if start >= end {
            return Err(errors::ErrorKind::InvalidParameter(
                "Expected Start < End".to_string(),
            ));
        }

        if n == 0 {
            return Err(errors::ErrorKind::InvalidParameter(
                "Expected N > 0".to_string(),
            ));
        }

        let n_float: T = NumCast::from(n - 1).unwrap();
        let dx = (end - start) / n_float;
        let mut storage_val = vec![start; n as usize];
        for i in 1..(n as usize) {
            storage_val[i] = storage_val[i - 1] + NumCast::from(dx).unwrap();
        }
        let storage_val: Vec<T> = storage_val.iter().map(|x| base.powf(*x)).collect();

        let len = storage_val.len();
        let new_storage = Rc::new(RefCell::new(storage::Storage::new(storage_val)));

        Ok(Tensor::<T>::from_raw(
            new_storage,
            vec![len as u64],
            types::type_from_range(
                <T as Bounded>::min_value().to_f64().unwrap(),
                <T as Bounded>::max_value().to_f64().unwrap(),
            )
            .unwrap(),
            vec![1u64],
            0,
        ))
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn create_from_val() {
        let zeros = Tensor::<i32>::zeros(vec![2, 2, 2]).unwrap();
        assert!(zeros
            .get_storage_ptr()
            .borrow()
            .get_vec()
            .iter()
            .all(|x| { x == &0i32 }));
        assert_eq!(zeros.get_storage_ptr().borrow().get_vec().len(), 8);
        assert_eq!(*zeros.get_strides(), vec![4u64, 2u64, 1u64]);
        assert_eq!(zeros.get_offset(), 0);

        let ones = Tensor::<i32>::ones(vec![2, 2, 2, 3]).unwrap();
        assert!(ones
            .get_storage_ptr()
            .borrow()
            .get_vec()
            .iter()
            .all(|x| { x == &1i32 }));
        assert_eq!(ones.get_storage_ptr().borrow().get_vec().len(), 24);
        assert_eq!(*ones.get_strides(), vec![12u64, 6u64, 3u64, 1u64]);
        assert_eq!(ones.get_offset(), 0);

        let rval = Tensor::<i32>::from_val(42, vec![7, 2, 5]).unwrap();
        assert!(rval
            .get_storage_ptr()
            .borrow()
            .get_vec()
            .iter()
            .all(|x| { x == &42i32 }));
        assert_eq!(rval.get_storage_ptr().borrow().get_vec().len(), 70);
        assert_eq!(*rval.get_strides(), vec![10u64, 5u64, 1u64]);
        assert_eq!(rval.get_offset(), 0);
    }

    #[test]
    fn arange() {
        let arangeu8 = Tensor::<u8>::arange(0, 10, 1).unwrap();
        let storageu8 = arangeu8.get_storage_ptr().borrow();
        let mut iteru8 = storageu8.get_vec().iter();
        assert_eq!(iteru8.next(), Some(&0u8));
        assert_eq!(iteru8.next(), Some(&1u8));
        assert_eq!(iteru8.next(), Some(&2u8));
        assert_eq!(iteru8.next(), Some(&3u8));
        assert_eq!(iteru8.next(), Some(&4u8));
        assert_eq!(iteru8.next(), Some(&5u8));
        assert_eq!(iteru8.next(), Some(&6u8));
        assert_eq!(iteru8.next(), Some(&7u8));
        assert_eq!(iteru8.next(), Some(&8u8));
        assert_eq!(iteru8.next(), Some(&9u8));
        assert_eq!(iteru8.next(), None);
        assert_eq!(*arangeu8.get_dim(), vec![10u64]);
        assert_eq!(*arangeu8.get_strides(), vec![1u64]);
        assert_eq!(arangeu8.get_offset(), 0);

        let arangei32 = Tensor::<i32>::arange(-15, 31, 5).unwrap();
        let storagei32 = arangei32.get_storage_ptr().borrow();
        let mut iteri32 = storagei32.get_vec().iter();
        assert_eq!(iteri32.next(), Some(&-15i32));
        assert_eq!(iteri32.next(), Some(&-10i32));
        assert_eq!(iteri32.next(), Some(&-5i32));
        assert_eq!(iteri32.next(), Some(&0i32));
        assert_eq!(iteri32.next(), Some(&5i32));
        assert_eq!(iteri32.next(), Some(&10i32));
        assert_eq!(iteri32.next(), Some(&15i32));
        assert_eq!(iteri32.next(), Some(&20i32));
        assert_eq!(iteri32.next(), Some(&25i32));
        assert_eq!(iteri32.next(), Some(&30i32));
        assert_eq!(iteri32.next(), None);
        assert_eq!(*arangei32.get_dim(), vec![10u64]);
        assert_eq!(*arangei32.get_strides(), vec![1u64]);
        assert_eq!(arangei32.get_offset(), 0);

        let arangef64 = Tensor::<f64>::arange(0.0, 2.3, 0.3).unwrap();
        let storagef64 = arangef64.get_storage_ptr().borrow();
        let stf64_vec = storagef64.get_vec();
        let expected = [0.0, 0.3, 0.6, 0.9, 1.2, 1.5, 1.8, 2.1];
        assert!(stf64_vec.len() == expected.len());
        (0..stf64_vec.len()).for_each(|x| {
            assert!((stf64_vec[x] - expected[x]).abs() < 1e-7);
        });
        assert_eq!(*arangef64.get_dim(), vec![8u64]);
        assert_eq!(*arangef64.get_strides(), vec![1u64]);
        assert_eq!(arangef64.get_offset(), 0);
    }

    #[test]
    fn arange_invalid_parameter() {
        let arangei32 = Tensor::<i32>::arange(71, 31, 5);
        assert!(match arangei32 {
            Ok(_) => false,
            Err(e) => match e {
                errors::ErrorKind::InvalidParameter(_) => true,
                _ => false,
            },
        });

        let arangef64 = Tensor::<f64>::arange(2.4, 2.3, 0.3);
        assert!(match arangef64 {
            Ok(_) => false,
            Err(e) => match e {
                errors::ErrorKind::InvalidParameter(_) => true,
                _ => false,
            },
        });
    }

    #[test]
    fn linspace() {
        let linespacef32 = Tensor::<f32>::linspace(0.0, 1.0, 10).unwrap();
        let storagef32 = linespacef32.get_storage_ptr().borrow();
        let stf32_vec = storagef32.get_vec();
        let expected = [
            0.0f32,
            0.1111111111111f32,
            0.2222222222222f32,
            0.3333333333333f32,
            0.4444444444444f32,
            0.5555555555555f32,
            0.6666666666666f32,
            0.7777777777777f32,
            0.8888888888888f32,
            0.9999999999999f32,
        ];
        assert!(stf32_vec.len() == expected.len());
        (0..stf32_vec.len()).for_each(|x| {
            assert!((stf32_vec[x] - expected[x]).abs() < 1e-7);
        });
        assert_eq!(*linespacef32.get_dim(), vec![10u64]);
        assert_eq!(*linespacef32.get_strides(), vec![1u64]);
        assert_eq!(linespacef32.get_offset(), 0);
        assert!(match linespacef32.get_dtype() {
            types::Types::Float32 => true,
            _ => false,
        });
    }

    #[test]
    fn linspace_invalid_parameters() {
        let linespacef32 = Tensor::<f32>::linspace(0.0, 1.0, 0);
        assert!(match linespacef32 {
            Ok(_) => false,
            Err(e) => match e {
                errors::ErrorKind::InvalidParameter(_) => true,
                _ => false,
            },
        });

        let linespacef64 = Tensor::<f64>::linspace(1.0, 0.0, 100);
        assert!(match linespacef64 {
            Ok(_) => false,
            Err(e) => match e {
                errors::ErrorKind::InvalidParameter(_) => true,
                _ => false,
            },
        });
    }

    #[test]
    fn logspace() {
        let logspacef64 = Tensor::<f64>::logspace(0.1, 1.0, 5, 10.0).unwrap();
        let storagef64 = logspacef64.get_storage_ptr().borrow();
        let stf64_vec = storagef64.get_vec();
        let expected = [
            1.25892541f64,
            2.11348904f64,
            3.54813389f64,
            5.95662144f64,
            10.0f64,
        ];
        assert!(stf64_vec.len() == expected.len());
        (0..stf64_vec.len()).for_each(|x| {
            assert!((stf64_vec[x] - expected[x]).abs() < 1e-7);
        });
        assert_eq!(*logspacef64.get_dim(), vec![5u64]);
        assert_eq!(*logspacef64.get_strides(), vec![1u64]);
        assert_eq!(logspacef64.get_offset(), 0);
        assert!(match logspacef64.get_dtype() {
            types::Types::Float64 => true,
            _ => false,
        });
    }

    #[test]
    fn logspace_invalid_parameters() {
        let logspacef32 = Tensor::<f32>::logspace(0.0, 1.0, 0, 10.0);
        assert!(match logspacef32 {
            Ok(_) => false,
            Err(e) => match e {
                errors::ErrorKind::InvalidParameter(_) => true,
                _ => false,
            },
        });

        let logspacef64 = Tensor::<f64>::logspace(1.0, 0.0, 100, 10.0);
        assert!(match logspacef64 {
            Ok(_) => false,
            Err(e) => match e {
                errors::ErrorKind::InvalidParameter(_) => true,
                _ => false,
            },
        });
    }

    #[test]
    fn eye() {
        let mat_u8 = Tensor::<u8>::eye(3, 3).unwrap();
        assert_eq!(
            mat_u8.get_storage_ptr().borrow().get_vec(),
            &vec![1u8, 0u8, 0u8, 0u8, 1u8, 0u8, 0u8, 0u8, 1u8]
        );
        assert_eq!(*mat_u8.get_dim(), vec![3u64, 3u64]);
        assert_eq!(*mat_u8.get_strides(), vec![3u64, 1u64]);
        assert_eq!(mat_u8.get_offset(), 0);
        assert!(match mat_u8.get_dtype() {
            types::Types::Uint8 => true,
            _ => false,
        });

        let mat_i128 = Tensor::<i128>::eye(3, 6).unwrap();
        let storage_i128 = mat_i128.get_storage_ptr().borrow();
        assert_eq!(
            storage_i128.get_vec(),
            &vec![
                1i128, 0i128, 0i128, 0i128, 0i128, 0i128, 0i128, 1i128, 0i128, 0i128, 0i128, 0i128,
                0i128, 0i128, 1i128, 0i128, 0i128, 0i128
            ]
        );
        assert_eq!(*mat_i128.get_dim(), vec![3u64, 6u64]);
        assert_eq!(*mat_i128.get_strides(), vec![6u64, 1u64]);
        assert_eq!(mat_i128.get_offset(), 0);
        assert!(match mat_i128.get_dtype() {
            types::Types::Int128 => true,
            _ => false,
        });

        let mat_f64 = Tensor::<f64>::eye(5, 2).unwrap();
        let storage_f64 = mat_f64.get_storage_ptr().borrow();
        let stvec_f64 = storage_f64.get_vec();
        let expected = [
            1.0f64, 0.0f64, 0.0f64, 1.0f64, 0.0f64, 0.0f64, 0.0f64, 0.0f64, 0.0f64, 0.0f64,
        ];
        assert!(stvec_f64.len() == expected.len());
        (0..stvec_f64.len()).for_each(|x| {
            assert!((stvec_f64[x] - expected[x]).abs() < 1e-7);
        });
        assert_eq!(*mat_f64.get_dim(), vec![5u64, 2u64]);
        assert_eq!(*mat_f64.get_strides(), vec![2u64, 1u64]);
        assert_eq!(mat_f64.get_offset(), 0);
        assert!(match mat_f64.get_dtype() {
            types::Types::Float64 => true,
            _ => false,
        });
    }

    #[test]
    fn eye_invalid_parameters() {
        let mat_u8 = Tensor::<u8>::eye(3, 0);
        assert!(match mat_u8 {
            Ok(_) => false,
            Err(e) => match e {
                errors::ErrorKind::InvalidParameter(_) => true,
                _ => false,
            },
        });

        let mat_i128 = Tensor::<i128>::eye(0, 3);
        assert!(match mat_i128 {
            Ok(_) => false,
            Err(e) => match e {
                errors::ErrorKind::InvalidParameter(_) => true,
                _ => false,
            },
        });
    }
}
