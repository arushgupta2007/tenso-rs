use num::{Bounded, NumCast, ToPrimitive};

use crate::errors;
use crate::types;

#[derive(Clone, Debug)]
pub struct Storage<T: ToPrimitive>(Vec<T>, types::Types);

pub(super) trait StorageTrait {
    type T: Bounded + ToPrimitive + NumCast + Copy;

    fn new(vals: Vec<Self::T>) -> Self;
    fn get_vec(&self) -> &Vec<Self::T>;
    fn upd_vec(&mut self, pos: u64, val: Self::T);
    fn at_vec(&self, pos: u64) -> &Self::T;
    fn len(&self) -> u64;
    fn get_type(&self) -> types::Types;

    fn size(&self) -> usize {
        self.get_vec().len()
    }

    fn to<DT: Bounded + ToPrimitive + NumCast>(&self) -> Result<Storage<DT>, errors::ErrorKind> {
        let vals = self.get_vec();
        let dtype = types::type_from_range(
            DT::min_value().to_f64().unwrap(),
            DT::max_value().to_f64().unwrap(),
        )?;
        if vals.iter().any(|x| {
            let cast = NumCast::from::<Self::T>(*x);
            if cast == None {
                return true;
            }

            let y: f64 = cast.unwrap();
            y < DT::min_value().to_f64().unwrap() || y > DT::max_value().to_f64().unwrap()
        }) {
            Err(errors::ErrorKind::CannotCastToDtype(
                format!(
                    "Cannot cast tensor of type {} to {}",
                    self.get_type(),
                    dtype
                )
                .to_string(),
            ))
        } else {
            let new_val: Vec<DT> = vals
                .iter()
                .map(|x| {
                    let cast = NumCast::from::<Self::T>(*x);
                    let y: DT = cast.unwrap();
                    y
                })
                .collect();
            Ok(Storage(new_val, dtype))
        }
    }
}

impl<Y: Bounded + ToPrimitive + NumCast + Copy> StorageTrait for Storage<Y> {
    type T = Y;

    fn new(vals: Vec<Self::T>) -> Self {
        Storage(
            vals,
            types::type_from_range(
                Y::min_value().to_f64().unwrap(),
                Y::max_value().to_f64().unwrap(),
            )
            .unwrap(),
        )
    }

    fn get_vec(&self) -> &Vec<Self::T> {
        &self.0
    }

    fn upd_vec(&mut self, pos: u64, val: Self::T) {
        self.0[pos as usize] = val;
    }

    fn at_vec(&self, pos: u64) -> &Y {
        &self.0[pos as usize]
    }

    fn get_type(&self) -> types::Types {
        self.1
    }

    fn len(&self) -> u64 {
        self.0.len() as u64
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn i8_to_u8_possible() -> Result<(), String> {
        let storage_arr: Vec<i8> = vec![0i8, 127i8, 3i8, 97i8, 54i8];
        let i8_storage = Storage::<i8>::new(storage_arr);
        let u8_storage = i8_storage.to::<u8>();
        match u8_storage {
            Ok(arr) => {
                let mut iter = arr.get_vec().iter();
                assert_eq!(iter.next(), Some(&0u8));
                assert_eq!(iter.next(), Some(&127u8));
                assert_eq!(iter.next(), Some(&3u8));
                assert_eq!(iter.next(), Some(&97u8));
                assert_eq!(iter.next(), Some(&54u8));
                Ok(())
            }
            Err(e) => Err(format!("{:#?}", e).to_string()),
        }
    }

    #[test]
    fn i8_to_u8_impossible() -> Result<(), String> {
        let storage_arr: Vec<i8> = vec![0i8, -127i8, -3i8, 97i8, 54i8];
        let i8_storage = Storage::<i8>::new(storage_arr);
        let u8_storage = i8_storage.to::<u8>();
        match u8_storage {
            Ok(_) => Err(String::from("Converted to u8 but values out of range")),
            Err(e) => match e {
                errors::ErrorKind::CannotCastToDtype(_) => Ok(()),
                _ => Err(String::from("Wrong Error Type")),
            },
        }
    }

    #[test]
    fn i16_to_u8_impossible() -> Result<(), String> {
        let storage_arr: Vec<i16> = vec![0i16, 255i16, 256i16, -255i16];
        let i8_storage = Storage::<i16>::new(storage_arr);
        let u8_storage = i8_storage.to::<u8>();
        match u8_storage {
            Ok(_) => Err(String::from("Converted to u8 but values out of range")),
            Err(e) => match e {
                errors::ErrorKind::CannotCastToDtype(_) => Ok(()),
                _ => Err(String::from("Wrong Error Type")),
            },
        }
    }

    #[test]
    fn f32_to_u32_possible() -> Result<(), String> {
        let storage_arr: Vec<f32> = vec![0.0f32, 47.0f32, 97.73f32, 224.5f32];
        let i8_storage = Storage::<f32>::new(storage_arr);
        let u8_storage = i8_storage.to::<u32>();
        match u8_storage {
            Ok(arr) => {
                let mut iter = arr.get_vec().iter();
                assert_eq!(iter.next(), Some(&0u32));
                assert_eq!(iter.next(), Some(&47u32));
                assert_eq!(iter.next(), Some(&97u32));
                assert_eq!(iter.next(), Some(&224u32));
                Ok(())
            }
            Err(_) => Err(String::from("Could not convert to u8 but values in range")),
        }
    }

    #[test]
    fn f32_to_u32_impossible() -> Result<(), String> {
        let storage_arr: Vec<f32> = vec![0.0f32, 47.0f32, -97.73f32, 4294967295.5f32];
        let i8_storage = Storage::<f32>::new(storage_arr);
        let u8_storage = i8_storage.to::<u32>();
        match u8_storage {
            Ok(_) => Err(String::from("Converted to u8 but values out of range")),
            Err(e) => match e {
                errors::ErrorKind::CannotCastToDtype(_) => Ok(()),
                _ => Err(String::from("Wrong Error Type")),
            },
        }
    }

    #[test]
    fn i16_to_i8_possible() -> Result<(), String> {
        let storage_arr: Vec<i16> = vec![0i16, 127i16, -127i16, 49i16, 37i16, 29i16];
        let i8_storage = Storage::<i16>::new(storage_arr);
        let u8_storage = i8_storage.to::<i8>();
        match u8_storage {
            Ok(arr) => {
                let mut iter = arr.get_vec().iter();
                assert_eq!(iter.next(), Some(&0i8));
                assert_eq!(iter.next(), Some(&127i8));
                assert_eq!(iter.next(), Some(&-127i8));
                assert_eq!(iter.next(), Some(&49i8));
                assert_eq!(iter.next(), Some(&37i8));
                assert_eq!(iter.next(), Some(&29i8));
                Ok(())
            }
            Err(_) => Err(String::from("Could not convert to i8 but values in range")),
        }
    }

    #[test]
    fn i16_to_i8_impossible() -> Result<(), String> {
        let storage_arr: Vec<i16> = vec![0i16, 128i16, -128i16, 49i16, 37i16, 29i16];
        let i8_storage = Storage::<i16>::new(storage_arr);
        let u8_storage = i8_storage.to::<i8>();
        match u8_storage {
            Ok(_) => Err(String::from("Converted to i16 but values out of range")),
            Err(e) => match e {
                errors::ErrorKind::CannotCastToDtype(_) => Ok(()),
                _ => Err(String::from("Wrong Error Type")),
            },
        }
    }

    #[test]
    fn f64_to_f32_possible() -> Result<(), String> {
        let storage_arr: Vec<f64> = vec![-1.34567345e37, 1.74563854e37, 5433453.345763454];
        let i8_storage = Storage::<f64>::new(storage_arr);
        let u8_storage = i8_storage.to::<f32>();
        match u8_storage {
            Ok(arr) => {
                let mut iter = arr.get_vec().iter();
                assert_eq!(iter.next(), Some(&-1.34567345e37f32));
                assert_eq!(iter.next(), Some(&1.74563854e37f32));
                assert_eq!(iter.next(), Some(&5433453.345763454f32));
                Ok(())
            }
            Err(_) => Err(String::from("Could not convert to f32 but values in range")),
        }
    }

    #[test]
    fn f64_to_f32_impossible() -> Result<(), String> {
        let storage_arr: Vec<f64> = vec![-1.34567345e40, 1.74563854e40, 5433453.345763454];
        let i8_storage = Storage::<f64>::new(storage_arr);
        let u8_storage = i8_storage.to::<f32>();
        match u8_storage {
            Ok(_) => Err(String::from("Converted to f32 but values out of range")),
            Err(e) => match e {
                errors::ErrorKind::CannotCastToDtype(_) => Ok(()),
                _ => Err(String::from("Wrong Error Type")),
            },
        }
    }

    #[test]
    fn u128_to_i8_possible() -> Result<(), String> {
        let storage_arr: Vec<u128> = vec![0u128, 127u128, 23u128, 97u128];
        let i8_storage = Storage::<u128>::new(storage_arr);
        let u8_storage = i8_storage.to::<i8>();
        match u8_storage {
            Ok(arr) => {
                let mut iter = arr.get_vec().iter();
                assert_eq!(iter.next(), Some(&0i8));
                assert_eq!(iter.next(), Some(&127i8));
                assert_eq!(iter.next(), Some(&23i8));
                assert_eq!(iter.next(), Some(&97i8));
                Ok(())
            }
            Err(e) => Err(format!("{:#?}", e).to_string()),
        }
    }

    #[test]
    fn u128_to_i8_impossible() -> Result<(), String> {
        let storage_arr: Vec<u128> = vec![0u128, 255u128, 128u128, 23u128];
        let i8_storage = Storage::<u128>::new(storage_arr);
        let u8_storage = i8_storage.to::<i8>();
        match u8_storage {
            Ok(_) => Err(String::from("Converted to i8 but values out of range")),
            Err(e) => match e {
                errors::ErrorKind::CannotCastToDtype(_) => Ok(()),
                _ => Err(String::from("Wrong Error Type")),
            },
        }
    }

    #[test]
    fn i16_to_f32_possible() -> Result<(), String> {
        let storage_arr: Vec<i16> = vec![0i16, 127i16, -127i16, 49i16, 37i16, 29i16];
        let i8_storage = Storage::<i16>::new(storage_arr);
        let u8_storage = i8_storage.to::<f32>();
        match u8_storage {
            Ok(arr) => {
                let mut iter = arr.get_vec().iter();
                assert_eq!(iter.next(), Some(&0.0f32));
                assert_eq!(iter.next(), Some(&127.0f32));
                assert_eq!(iter.next(), Some(&-127.0f32));
                assert_eq!(iter.next(), Some(&49.0f32));
                assert_eq!(iter.next(), Some(&37.0f32));
                assert_eq!(iter.next(), Some(&29.0f32));
                Ok(())
            }
            Err(_) => Err(String::from("Could not convert to i8 but values in range")),
        }
    }
}
