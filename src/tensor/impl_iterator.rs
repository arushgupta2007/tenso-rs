use std::ops;

use num::{Bounded, NumCast, ToPrimitive};

use super::Tensor;

impl<'a, T: Bounded + NumCast + ToPrimitive + Copy + PartialOrd + ops::AddAssign> IntoIterator
    for &'a Tensor<T>
{
    type Item = T;
    type IntoIter = TensorIterator<'a, T>;

    fn into_iter(self) -> Self::IntoIter {
        TensorIterator {
            tensor: &self,
            dim_index: vec![0; self.no_dim() as usize],
            done: false,
        }
    }
}

pub struct TensorIterator<'a, T: ToPrimitive> {
    tensor: &'a Tensor<T>,
    dim_index: Vec<u64>,
    done: bool,
}

impl<'a, T: Bounded + NumCast + ToPrimitive + Copy + PartialOrd + ops::AddAssign> Iterator
    for TensorIterator<'a, T>
{
    type Item = T;
    fn next(&mut self) -> Option<Self::Item> {
        if self.done {
            return None;
        }

        let ret = self.tensor.get_at_dim_index_unchecked(&self.dim_index);

        while !self.dim_index.is_empty()
            && *self.dim_index.last().unwrap()
                == self.tensor.get_dim()[self.dim_index.len() - 1] - 1
        {
            self.dim_index.pop();
        }

        if !self.dim_index.is_empty() {
            *self.dim_index.last_mut().unwrap() += 1;
            while self.dim_index.len() < self.tensor.no_dim() as usize {
                self.dim_index.push(0);
            }
        } else {
            self.done = true;
        }

        return Some(ret);
    }
}

#[cfg(test)]
mod test {
    use crate::tensor::Tensor;

    #[test]
    fn iterator() {
        let t = Tensor::<i32>::arange(0, 27, 1)
            .unwrap()
            .reshape(vec![3, 3, 3])
            .unwrap();
        let iter = t.into_iter();
        assert!(iter.enumerate().all(|(exp_val, val)| val == exp_val as i32));

        let arr = vec![32, 346, 34, 656, 78, 12, 567, 785];
        let t1 = Tensor::<i32>::from_array(arr.clone(), vec![2, 2, 2]).unwrap();
        let iter1 = t1.into_iter();
        assert!(iter1.zip(arr.iter()).all(|(val, exp_val)| val == *exp_val));

        let t2 = t.get(vec![1..3, 1..3, 1..3]).unwrap();
        let iter2 = t2.into_iter();
        let exp = vec![13, 14, 16, 17, 22, 23, 25, 26];
        assert!(iter2.zip(exp.iter()).all(|(val, exp_val)| val == *exp_val));
    }
}
