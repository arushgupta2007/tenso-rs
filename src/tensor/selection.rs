use std::{
    collections::BTreeSet,
    fmt::{Debug, Display},
    ops,
};

use num::{Bounded, NumCast, ToPrimitive};

use crate::errors;

use super::{utils, Tensor};

pub struct TensorSelection<'a, T: ToPrimitive> {
    tensor: &'a Tensor<T>,
    selection: BTreeSet<u64>,
}

fn disallow_zero_dim_and_empty_tensors<
    T: Bounded + NumCast + ToPrimitive + Copy + PartialOrd + ops::AddAssign,
>(
    tensor: &Tensor<T>,
) -> Result<(), errors::ErrorKind> {
    if tensor.no_dim() == 0 {
        return Err(errors::ErrorKind::InvalidParameter(
            "Invalid Tensor Dimensions, expected >= 1, got: 0".to_string(),
        ));
    }

    if tensor.num_el() == 0 {
        return Err(errors::ErrorKind::InvalidParameter(
            "Invalid Tensor Size, expected >= 1 elements, got: 0 elements".to_string(),
        ));
    }

    Ok(())
}

impl<'a, T: Bounded + NumCast + ToPrimitive + Copy + PartialOrd + ops::AddAssign>
    TensorSelection<'a, T>
{
    fn check_index(&self, idx: u64) -> Result<(), errors::ErrorKind> {
        let no_el = self.tensor.num_el();
        if idx >= no_el {
            return Err(errors::ErrorKind::InvalidParameter(
                format!("Index out of bounds, expected: < {}, got: {}", no_el, idx).to_string(),
            ));
        }
        Ok(())
    }

    pub(super) fn new(tensor: &Tensor<T>) -> TensorSelection<T> {
        TensorSelection {
            tensor,
            selection: BTreeSet::<u64>::new(),
        }
    }

    pub fn from_indicies(
        tensor: &Tensor<T>,
        selection_vec: Vec<u64>,
    ) -> Result<TensorSelection<T>, errors::ErrorKind> {
        disallow_zero_dim_and_empty_tensors(tensor)?;

        let no_el = tensor.num_el();
        if selection_vec.iter().any(|x| *x >= no_el) {
            return Err(errors::ErrorKind::InvalidParameter(
                "Indicies out of bounds".to_string(),
            ));
        }

        let mut selection = BTreeSet::new();
        selection_vec.iter().for_each(|x| {
            selection.insert(*x);
        });

        Ok(TensorSelection { tensor, selection })
    }

    pub fn from_mask(
        tensor: &Tensor<T>,
        mask: Vec<bool>,
    ) -> Result<TensorSelection<T>, errors::ErrorKind> {
        disallow_zero_dim_and_empty_tensors(tensor)?;

        let no_el = tensor.num_el();
        if mask.len() as u64 != no_el {
            return Err(errors::ErrorKind::InvalidParameter(
                format!(
                    "Invalid Mask Length, expected: {}, got: {}",
                    no_el,
                    mask.len()
                )
                .to_string(),
            ));
        }

        let selection_vec: Vec<u64> = mask
            .iter()
            .enumerate()
            .filter(|(_, m)| **m)
            .map(|(idx, _)| idx as u64)
            .collect();

        let mut selection = BTreeSet::new();
        selection_vec.iter().for_each(|x| {
            selection.insert(*x);
        });

        Ok(TensorSelection { tensor, selection })
    }

    pub fn to_tensor(&self) -> Tensor<u64> {
        let mut storage_val = vec![];
        self.selection.iter().for_each(|idx| {
            let dim_idx = self.tensor.dim_index_from_index_unchecked(*idx);
            storage_val.extend(&dim_idx);
        });

        Tensor::<u64>::from_array(
            storage_val,
            vec![self.selection.len() as u64, self.tensor.no_dim()],
        )
        .unwrap()
    }

    pub fn toggle_index(&mut self, idx: u64) -> Result<&mut Self, errors::ErrorKind> {
        self.check_index(idx)?;

        if self.selection.contains(&idx) {
            self.selection.remove(&idx);
        } else {
            self.selection.insert(idx);
        }

        Ok(self)
    }

    pub fn toggle_dim_index(&mut self, dim_idx: Vec<u64>) -> Result<&mut Self, errors::ErrorKind> {
        let dim_stride = utils::strides_from_dimensions(self.tensor.get_dim().clone());
        let idx = dim_stride.iter().zip(dim_idx).map(|(s, i)| s * i).sum();
        self.toggle_index(idx)
    }

    pub fn select_index(&mut self, idx: u64) -> Result<&mut Self, errors::ErrorKind> {
        self.check_index(idx)?;

        self.selection.insert(idx);

        Ok(self)
    }

    pub fn select_dim_index(&mut self, dim_idx: Vec<u64>) -> Result<&mut Self, errors::ErrorKind> {
        let dim_stride = utils::strides_from_dimensions(self.tensor.get_dim().clone());
        let idx = dim_stride.iter().zip(dim_idx).map(|(s, i)| s * i).sum();
        self.select_index(idx)
    }

    pub fn unselect_index(&mut self, idx: u64) -> Result<&mut Self, errors::ErrorKind> {
        self.check_index(idx)?;

        self.selection.remove(&idx);

        Ok(self)
    }

    pub fn unselect_dim_index(
        &mut self,
        dim_idx: Vec<u64>,
    ) -> Result<&mut Self, errors::ErrorKind> {
        let dim_stride = utils::strides_from_dimensions(self.tensor.get_dim().clone());
        let idx = dim_stride.iter().zip(dim_idx).map(|(s, i)| s * i).sum();
        self.unselect_index(idx)
    }

    pub fn toggle_where(&mut self, condition: impl Fn(Vec<u64>, T) -> bool) -> &mut Self {
        self.tensor.into_iter().enumerate().for_each(|(idx, val)| {
            let idx_u64 = idx as u64;
            let dim_idx = self.tensor.dim_index_from_index_unchecked(idx_u64);
            if condition(dim_idx, val) {
                if self.selection.contains(&idx_u64) {
                    self.selection.remove(&idx_u64);
                } else {
                    self.selection.insert(idx_u64);
                }
            }
        });

        self
    }

    pub fn select_where(&mut self, condition: impl Fn(Vec<u64>, T) -> bool) -> &mut Self {
        self.tensor.into_iter().enumerate().for_each(|(idx, val)| {
            let idx_u64 = idx as u64;
            let dim_idx = self.tensor.dim_index_from_index_unchecked(idx_u64);
            if condition(dim_idx, val) {
                self.selection.insert(idx_u64);
            }
        });

        self
    }

    pub fn unselect_where(&mut self, condition: impl Fn(Vec<u64>, T) -> bool) -> &mut Self {
        self.tensor.into_iter().enumerate().for_each(|(idx, val)| {
            let idx_u64 = idx as u64;
            let dim_idx = self.tensor.dim_index_from_index_unchecked(idx_u64);
            if condition(dim_idx, val) {
                self.selection.remove(&idx_u64);
            }
        });

        self
    }
}

impl<'a, T: Bounded + NumCast + ToPrimitive + Copy + PartialOrd + ops::AddAssign + Display> Debug
    for TensorSelection<'a, T>
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            format!(
                "TensorSelection (tensor={:?}, selection={:?})",
                self.tensor, self.selection
            )
        )
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn _index() {
        let t = Tensor::<i32>::arange(0, 27, 1)
            .unwrap()
            .reshape(vec![3, 3, 3])
            .unwrap();
        let mut ts = TensorSelection::new(&t);
        ts.toggle_index(13)
            .unwrap()
            .select_dim_index(vec![0, 2, 2])
            .unwrap();
        assert_eq!(ts.selection, BTreeSet::from([8, 13]));
        ts.unselect_index(8)
            .unwrap()
            .unselect_dim_index(vec![1, 1, 1])
            .unwrap();
        assert_eq!(ts.selection, BTreeSet::new());

        let t1 = t.get(vec![1..3, 1..3, 1..3]).unwrap();
        let mut ts1 = TensorSelection::new(&t1);
        ts1.select_index(0)
            .unwrap()
            .toggle_dim_index(vec![1, 1, 1])
            .unwrap();
        assert_eq!(ts1.selection, BTreeSet::from([0, 7]));
        ts1.unselect_index(0)
            .unwrap()
            .toggle_index(7)
            .unwrap()
            .select_dim_index(vec![0, 0, 1])
            .unwrap()
            .select_index(6)
            .unwrap();
        assert_eq!(ts1.selection, BTreeSet::from([1, 6]));
    }

    #[test]
    fn _where() {
        let t = Tensor::<i32>::arange(0, 27, 1)
            .unwrap()
            .reshape(vec![3, 3, 3])
            .unwrap();

        let mut ts = TensorSelection::new(&t);
        ts.toggle_where(|_, val| val > 10);
        assert_eq!(
            ts.selection,
            BTreeSet::from([11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26])
        );
        ts.unselect_where(|_, val| val > 20);
        assert_eq!(
            ts.selection,
            BTreeSet::from([11, 12, 13, 14, 15, 16, 17, 18, 19, 20])
        );
        ts.select_where(|dim_idx, _| dim_idx.iter().sum::<u64>() % 2 == 0);
        assert_eq!(
            ts.selection,
            BTreeSet::from([0, 2, 4, 6, 8, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 22, 24, 26])
        );

        let t1 = t.get(vec![1..3, 1..3, 1..3]).unwrap();
        let mut ts1 = TensorSelection::new(&t1);
        ts1.toggle_where(|_, val| val % 2 == 0);
        assert_eq!(ts1.selection, BTreeSet::from([1, 2, 4, 7]));
        ts1.unselect_where(|dim_idx, _| dim_idx.last().unwrap() > &0);
        assert_eq!(ts1.selection, BTreeSet::from([2, 4]));
        ts1.select_where(|dim_idx, _| dim_idx.last().unwrap() == &0);
        assert_eq!(ts1.selection, BTreeSet::from([0, 2, 4, 6]));
    }
}
