use std::{
    cell::RefCell,
    ops::{self, Range},
    rc::Rc,
};

use num::{Bounded, NumCast, ToPrimitive};

use crate::{errors, types};

use super::{
    selection::TensorSelection,
    storage::{self, StorageTrait},
    utils, Tensor,
};

impl<T: Bounded + NumCast + ToPrimitive + Copy + PartialOrd + ops::AddAssign> Tensor<T> {
    pub fn adjoint(&self) -> Result<Tensor<T>, errors::ErrorKind> {
        self.transpose(self.no_dim() - 1, self.no_dim() - 2)
    }

    pub fn argwhere(&self) -> Tensor<u64> {
        let zero: T = NumCast::from(0).unwrap();
        TensorSelection::new(self)
            .select_where(|_, val| !utils::eq::<T>(val, zero, self.get_dtype()))
            .to_tensor()
    }

    pub fn cat(&self, other: &Tensor<T>, axis: u64) -> Result<Tensor<T>, errors::ErrorKind> {
        if other.no_dim() != self.no_dim() {
            return Err(errors::ErrorKind::InvalidParameter(
                format!(
                    "Number of Dimensions do not match, expected: {}, got: {}",
                    self.no_dim(),
                    other.no_dim()
                )
                .to_string(),
            ));
        }

        if axis >= self.no_dim() {
            return Err(errors::ErrorKind::InvalidParameter(
                format!(
                    "Axis out of bounds, expected: < {}, got: {}",
                    self.no_dim(),
                    axis
                )
                .to_string(),
            ));
        }

        let other_dim = other.get_dim();

        if self.get_dim().contains(&0) || other_dim.contains(&0) {
            return Err(errors::ErrorKind::InvalidParameter(
                "Expected Dimensions sizes > zero".to_string(),
            ));
        }

        if (0..self.no_dim() as usize)
            .any(|idx| idx as u64 != axis && self.get_dim()[idx] != other_dim[idx])
        {
            return Err(errors::ErrorKind::InvalidParameter(
                "Dimensions do not match. Every dimension (except the 'axis') should be equal"
                    .to_string(),
            ));
        }

        Ok(self.cat_unchecked(other, axis))
    }

    pub fn cat_unchecked(&self, other: &Tensor<T>, axis: u64) -> Tensor<T> {
        let other_dim = other.get_dim();
        let other_stride = other.get_strides();

        let mut curr_data_iter = self.into_iter();
        let mut other_data_iter = other.into_iter();

        let mut data = Vec::with_capacity((self.num_el() + other.num_el()) as usize);
        let mut stack = vec![0; self.no_dim() as usize];
        while !stack.is_empty() {
            data.push(curr_data_iter.next().unwrap());

            while !stack.is_empty() && *stack.last().unwrap() == self.get_dim()[stack.len() - 1] - 1
            {
                if axis as usize == stack.len() - 1 {
                    for _ in 0..other_stride[axis as usize] * other_dim[axis as usize] {
                        data.push(other_data_iter.next().unwrap());
                    }
                }
                stack.pop();
            }

            if !stack.is_empty() {
                *stack.last_mut().unwrap() += 1;
                while stack.len() < self.no_dim() as usize {
                    stack.push(0);
                }
            }
        }

        let final_dimensions: Vec<u64> = (0..self.no_dim())
            .map(|x| {
                if x == axis {
                    self.get_dim()[x as usize] + other_dim[x as usize]
                } else {
                    self.get_dim()[x as usize]
                }
            })
            .collect();
        let final_stride = utils::strides_from_dimensions(final_dimensions.clone());
        let storage = Rc::new(RefCell::new(storage::Storage::new(data)));

        Tensor::from_raw(storage, final_dimensions, self.get_dtype(), final_stride, 0)
    }

    pub fn concat(&self, other: &Tensor<T>, axis: u64) -> Result<Tensor<T>, errors::ErrorKind> {
        self.cat(other, axis)
    }

    pub fn concatenate(
        &self,
        other: &Tensor<T>,
        axis: u64,
    ) -> Result<Tensor<T>, errors::ErrorKind> {
        self.cat(other, axis)
    }

    pub fn dsplit(&self, chunks: u64) -> Result<Vec<Tensor<T>>, errors::ErrorKind> {
        let axis = 2;

        if self.get_dim().contains(&0) {
            return Err(errors::ErrorKind::InvalidParameter(
                "Expected Dimensions size > zero".to_string(),
            ));
        }

        if chunks == 0 {
            return Err(errors::ErrorKind::InvalidParameter(
                "Expected Number of Chunks to be > zero".to_string(),
            ));
        }

        if axis >= self.no_dim() {
            return Err(errors::ErrorKind::InvalidParameter(
                format!("Invalid axis, expected: < {}, got: {}", self.no_dim(), axis).to_string(),
            ));
        }

        if chunks >= self.get_dim()[axis as usize] {
            return Err(errors::ErrorKind::InvalidParameter(
                format!(
                    "Invalid chunk size, expected: < {}, got: {}",
                    self.get_dim()[axis as usize],
                    chunks
                )
                .to_string(),
            ));
        }

        if self.get_dim()[axis as usize] % chunks != 0 {
            return Err(errors::ErrorKind::InvalidParameter(
                "Invalid chunk size, cannot divide evenly".to_string(),
            ));
        }

        Ok(self.dsplit_unchecked(chunks))
    }

    pub fn dsplit_unchecked(&self, chunks: u64) -> Vec<Tensor<T>> {
        self.tensor_split_unchecked(chunks, 2)
    }

    pub fn dsplit_index(
        &self,
        chunk_indicies: Vec<u64>,
    ) -> Result<Vec<Tensor<T>>, errors::ErrorKind> {
        self.tensor_split_index(chunk_indicies, 2)
    }

    pub fn dsplit_index_unchecked(&self, chunk_indicies: Vec<u64>) -> Vec<Tensor<T>> {
        self.tensor_split_index_unchecked(chunk_indicies, 2)
    }

    pub fn column_stack(&self, other: &Tensor<T>) -> Result<Tensor<T>, errors::ErrorKind> {
        self.atleast_2d().cat(&other.atleast_2d(), 1)
    }

    pub fn dstack(&self, other: &Tensor<T>) -> Result<Tensor<T>, errors::ErrorKind> {
        self.atleast_3d().cat(&other.atleast_3d(), 2)
    }

    pub fn gather(&self) {
        todo!()
    }

    pub fn hsplit(&self, chunks: u64, axis: u64) -> Result<Vec<Tensor<T>>, errors::ErrorKind> {
        self.tensor_split(chunks, axis)
    }

    pub fn hsplit_index(
        &self,
        chunk_indicies: Vec<u64>,
        axis: u64,
    ) -> Result<Vec<Tensor<T>>, errors::ErrorKind> {
        self.tensor_split_index(chunk_indicies, axis)
    }

    pub fn hstack(&self, other: &Tensor<T>) -> Result<Tensor<T>, errors::ErrorKind> {
        self.cat(other, 1)
    }

    pub fn index_add(&self) {
        todo!()
    }

    pub fn index_copy(&self) {
        todo!()
    }

    pub fn index_reduce(&self) {
        todo!()
    }

    pub fn masked_select(&self) {
        todo!()
    }

    pub fn movedim(&self, idx: u64, target: u64) -> Result<Tensor<T>, errors::ErrorKind> {
        if idx >= self.no_dim() {
            return Err(errors::ErrorKind::InvalidParameter(
                format!(
                    "Idx out of bounds, expected: < {}, got: {}",
                    self.no_dim(),
                    idx
                )
                .to_string(),
            ));
        }

        if target >= self.no_dim() {
            return Err(errors::ErrorKind::InvalidParameter(
                format!(
                    "Target out of bounds, expected: < {}, got: {}",
                    self.no_dim(),
                    target
                )
                .to_string(),
            ));
        }

        Ok(self.movedim_unchecked(idx, target))
    }

    pub fn movedim_unchecked(&self, idx: u64, target: u64) -> Tensor<T> {
        let mut permutation: Vec<u64> = (0..idx).chain((idx + 1)..self.no_dim()).collect();
        permutation.insert(target as usize, idx);

        self.permute_unchecked(permutation)
    }

    pub fn moveaxis(&self, idx: u64, target: u64) -> Result<Tensor<T>, errors::ErrorKind> {
        self.movedim(idx, target)
    }

    pub fn narrow(&self, dim: u64, start: u64, sz: u64) -> Result<Tensor<T>, errors::ErrorKind> {
        if dim >= self.no_dim() {
            return Err(errors::ErrorKind::InvalidParameter(
                format!(
                    "Dim out of bounds, expected: < {}, got: {}",
                    self.no_dim(),
                    dim
                )
                .to_string(),
            ));
        }

        if start + sz - 1 >= self.get_dim()[dim as usize] {
            return Err(errors::ErrorKind::InvalidParameter(
                format!(
                    "Range out of bounds, expected: < {}, got: {}..={}",
                    self.get_dim()[dim as usize],
                    start,
                    start + sz - 1
                )
                .to_string(),
            ));
        }

        if sz == 0 {
            return Err(errors::ErrorKind::InvalidParameter(
                "Invalid Sz, expected: > 0, got: 0".to_string(),
            ));
        }

        Ok(self.narrow_unchecked(dim, start, sz))
    }

    pub fn narrow_unchecked(&self, dim: u64, start: u64, sz: u64) -> Tensor<T> {
        let mut query_rngs: Vec<Range<u64>> = self.get_dim().iter().map(|x| 0..*x).collect();
        query_rngs[dim as usize] = start..(start + sz);

        self.get(query_rngs).unwrap()
    }

    pub fn nonzero(&self) -> Result<Vec<Tensor<u64>>, errors::ErrorKind> {
        let nz = self.atleast_1d().argwhere();
        let num_nonzero = nz.get_dim()[0];
        Ok(nz
            .tensor_split(self.no_dim(), 1)?
            .iter()
            .map(|ten| ten.t().unwrap().reshape_unchecked(vec![num_nonzero]))
            .collect())
    }

    pub fn permute(&self, permutation: Vec<u64>) -> Result<Tensor<T>, errors::ErrorKind> {
        if permutation.len() as u64 != self.no_dim() {
            return Err(errors::ErrorKind::InvalidParameter(
                format!(
                    "Invalid Permutation size, expected: {}, got: {}",
                    self.no_dim(),
                    permutation.len()
                )
                .to_string(),
            ));
        }

        let mut seen = vec![false; self.no_dim() as usize];
        for p in permutation.iter() {
            if *p >= self.no_dim() {
                return Err(errors::ErrorKind::InvalidParameter(
                    "Invalid permutation".to_string(),
                ));
            }
            seen[*p as usize] = true;
        }
        if !seen.iter().all(|x| *x) {
            return Err(errors::ErrorKind::InvalidParameter(
                "Invalid permutation".to_string(),
            ));
        }

        Ok(self.permute_unchecked(permutation))
    }

    pub fn permute_unchecked(&self, permutation: Vec<u64>) -> Tensor<T> {
        let new_dim: Vec<u64> = permutation
            .iter()
            .map(|x| self.get_dim()[*x as usize])
            .collect();
        let new_strides: Vec<u64> = permutation
            .iter()
            .map(|x| self.get_strides()[*x as usize])
            .collect();

        Tensor::<T>::from_raw(
            Rc::clone(self.get_storage_ptr()),
            new_dim,
            self.get_dtype(),
            new_strides,
            self.get_offset(),
        )
    }

    pub fn reshape(&self, dims: Vec<u64>) -> Result<Tensor<T>, errors::ErrorKind> {
        if *self.get_dim() == dims {
            return Ok(self.shallow_copy());
        }

        let no_elem: u64 = self.num_el();
        let new_no_elem: u64 = dims.iter().product();
        if no_elem != new_no_elem {
            return Err(errors::ErrorKind::InvalidParameter(format!("Invalid dimensions, attempting to reshape tensor of {} elements to one of {} elements", no_elem, new_no_elem).to_string()));
        }

        Ok(self.reshape_unchecked(dims))
    }

    pub fn reshape_unchecked(&self, dims: Vec<u64>) -> Tensor<T> {
        let no_elem: u64 = self.num_el();

        if self.is_contiguous() || no_elem == 1 {
            return Tensor::<T>::from_raw(
                Rc::clone(self.get_storage_ptr()),
                dims.clone(),
                self.get_dtype(),
                utils::strides_from_dimensions(dims.clone()),
                self.get_offset(),
            );
        }

        let curr_dim_wtout_1: Vec<u64> = self
            .get_dim()
            .iter()
            .filter(|x| *x > &1)
            .map(|x| *x)
            .collect();
        let new_dim_wtout_1: Vec<u64> = dims.iter().filter(|x| *x > &1).map(|x| *x).collect();

        if curr_dim_wtout_1 == new_dim_wtout_1 {
            // Do not need Deep copy
            let strides_wtout_1: Vec<u64> = self
                .get_strides()
                .iter()
                .zip(self.get_dim().iter())
                .filter(|(_, d)| *d > &1)
                .map(|(s, _)| *s)
                .collect();

            let mut pointer = (strides_wtout_1.len() - 1) as u64;
            let mut final_strides = vec![1u64; dims.len()];
            for idx in (0..dims.len()).rev() {
                if dims[idx] == 1 {
                    final_strides[idx] = *final_strides.get(idx + 1).unwrap_or_else(|| &1);
                } else {
                    final_strides[idx] = strides_wtout_1[pointer as usize];
                    if pointer > 0 {
                        pointer -= 1;
                    }
                }
            }

            return Tensor::<T>::from_raw(
                Rc::clone(self.get_storage_ptr()),
                dims.clone(),
                self.get_dtype(),
                final_strides,
                self.get_offset(),
            );
        }

        return Tensor::<T>::from_raw(
            Rc::clone(self.get_storage_ptr()),
            dims.clone(),
            self.get_dtype(),
            utils::strides_from_dimensions(dims.clone()),
            self.get_offset(),
        );
    }

    pub fn row_stack(&self, other: &Tensor<T>) -> Result<Tensor<T>, errors::ErrorKind> {
        self.vstack(other)
    }

    pub fn select(&self) -> TensorSelection<T> {
        TensorSelection::new(&self)
    }

    pub fn scatter(&self) {
        todo!()
    }

    pub fn diagonal_scatter(&self) {
        todo!()
    }

    pub fn select_scatter(&self) {
        todo!()
    }

    pub fn slice_scatter(&self) {
        todo!()
    }

    pub fn scatter_add(&self) {
        todo!()
    }

    pub fn scatter_reduce(&self) {
        todo!()
    }

    pub fn split(&self) {
        todo!()
    }

    pub fn squeeze(&self) {
        todo!()
    }

    pub fn stack(&self) {
        todo!()
    }

    pub fn swapaxes(&self) {
        todo!()
    }

    pub fn swapdims(&self) {
        todo!()
    }

    pub fn t(&self) -> Result<Tensor<T>, errors::ErrorKind> {
        if self.no_dim() > 2 {
            return Err(errors::ErrorKind::InvalidParameter(
                format!(
                    "Invalid Number of Dimensions, expected < 2, got: {}",
                    self.no_dim()
                )
                .to_string(),
            ));
        }

        self.atleast_2d().transpose(0, 1)
    }

    pub fn take(&self) {
        todo!()
    }

    pub fn take_along_dim(&self) {
        todo!()
    }

    pub fn tensor_split(
        &self,
        chunks: u64,
        axis: u64,
    ) -> Result<Vec<Tensor<T>>, errors::ErrorKind> {
        if self.get_dim().contains(&0) {
            return Err(errors::ErrorKind::InvalidParameter(
                "Expected Dimensions size > zero".to_string(),
            ));
        }

        if chunks == 0 {
            return Err(errors::ErrorKind::InvalidParameter(
                "Expected Number of Chunks to be > zero".to_string(),
            ));
        }

        if axis >= self.no_dim() {
            return Err(errors::ErrorKind::InvalidParameter(
                format!("Invalid axis, expected: < {}, got: {}", self.no_dim(), axis).to_string(),
            ));
        }

        if chunks > self.get_dim()[axis as usize] {
            return Err(errors::ErrorKind::InvalidParameter(
                format!(
                    "Invalid chunk size, expected: < {}, got: {}",
                    self.get_dim()[axis as usize],
                    chunks
                )
                .to_string(),
            ));
        }

        Ok(self.tensor_split_unchecked(chunks, axis))
    }

    pub fn tensor_split_unchecked(&self, chunks: u64, axis: u64) -> Vec<Tensor<T>> {
        let sz_1: u64 = self.get_dim()[axis as usize] / chunks;
        let sz_0 = if self.get_dim()[axis as usize] % chunks == 0 {
            sz_1
        } else {
            sz_1 + 1
        };

        let no_wt_sz_0 = self.get_dim()[axis as usize] % chunks;

        let mut prev = 0;
        let mut query_rngs: Vec<Range<u64>> = self.get_dim().iter().map(|x| 0..*x).collect();
        let mut res: Vec<Tensor<T>> = vec![];

        for idx in 0..chunks {
            if idx < no_wt_sz_0 {
                query_rngs[axis as usize] = prev..(prev + sz_0);
                prev += sz_0;
            } else {
                query_rngs[axis as usize] = prev..(prev + sz_1);
                prev += sz_1;
            }
            res.push(self.get(query_rngs.clone()).unwrap());
        }

        res
    }

    pub fn tensor_split_index(
        &self,
        chunk_indicies: Vec<u64>,
        axis: u64,
    ) -> Result<Vec<Tensor<T>>, errors::ErrorKind> {
        if axis >= self.no_dim() {
            return Err(errors::ErrorKind::InvalidParameter(
                format!("Invalid axis, expected: < {}, got: {}", self.no_dim(), axis).to_string(),
            ));
        }

        if self.get_dim().contains(&0) {
            return Err(errors::ErrorKind::InvalidParameter(
                "Expected Dimensions size > zero".to_string(),
            ));
        }

        let mut flag = true;
        for idx in 1..chunk_indicies.len() {
            if chunk_indicies[idx] <= chunk_indicies[idx - 1] {
                flag = false;
                break;
            }
        }

        if !flag {
            return Err(errors::ErrorKind::InvalidParameter(
                "Invalid Chunk Indicies, expected strictly increasing vector".to_string(),
            ));
        }

        if !chunk_indicies.is_empty()
            && *chunk_indicies.last().unwrap() >= self.get_dim()[axis as usize]
        {
            return Err(errors::ErrorKind::InvalidParameter(
                "Chunk Indicies out of bounds".to_string(),
            ));
        }

        Ok(self.tensor_split_index_unchecked(chunk_indicies, axis))
    }

    pub fn tensor_split_index_unchecked(
        &self,
        mut chunk_indicies: Vec<u64>,
        axis: u64,
    ) -> Vec<Tensor<T>> {
        if chunk_indicies.is_empty() || chunk_indicies[0] > 0 {
            chunk_indicies.insert(0, 0);
        }

        if chunk_indicies.is_empty()
            || *chunk_indicies.last().unwrap() < self.get_dim()[axis as usize]
        {
            chunk_indicies.push(self.get_dim()[axis as usize]);
        }

        let mut query_rngs: Vec<Range<u64>> = self.get_dim().iter().map(|x| 0..*x).collect();
        let mut res: Vec<Tensor<T>> = vec![];

        for idx in 1..chunk_indicies.len() {
            query_rngs[axis as usize].start = chunk_indicies[idx - 1];
            query_rngs[axis as usize].end = chunk_indicies[idx];

            res.push(self.get(query_rngs.clone()).unwrap());
        }

        res
    }

    pub fn tile(&self) {
        todo!()
    }

    pub fn to<Y: Bounded + NumCast + ToPrimitive + Copy + PartialOrd + ops::AddAssign>(
        &self,
    ) -> Result<Tensor<Y>, errors::ErrorKind> {
        if self.into_iter().any(|x| {
            let cast: f64 = NumCast::from(x).unwrap();
            cast < (Y::min_value().to_f64().unwrap()) || cast > (Y::max_value().to_f64().unwrap())
        }) {
            return Err(errors::ErrorKind::InvalidParameter(
                format!(
                    "Tensor values out of bounds of type: {}",
                    types::type_from_range(
                        Y::min_value().to_f64().unwrap(),
                        Y::max_value().to_f64().unwrap()
                    )
                    .unwrap()
                )
                .to_string(),
            ));
        }

        let data: Vec<Y> = self
            .into_iter()
            .map(|x| NumCast::from(x).unwrap())
            .collect();
        Tensor::<Y>::from_array(data, self.get_dim().clone())
    }

    pub fn transpose(&self, dim0: u64, dim1: u64) -> Result<Tensor<T>, errors::ErrorKind> {
        if dim0 >= self.no_dim() {
            return Err(errors::ErrorKind::InvalidParameter(
                format!(
                    "Dim0 out of bounds, expected: < {}, got: {}",
                    self.no_dim(),
                    dim0
                )
                .to_string(),
            ));
        }

        if dim1 >= self.no_dim() {
            return Err(errors::ErrorKind::InvalidParameter(
                format!(
                    "Dim1 out of bounds, expected: < {}, got: {}",
                    self.no_dim(),
                    dim1
                )
                .to_string(),
            ));
        }

        let mut permutation: Vec<u64> = (0..self.no_dim()).collect();
        permutation.swap(dim0 as usize, dim1 as usize);

        self.permute(permutation)
    }

    pub fn unbind(&self) {
        todo!()
    }

    pub fn unsqueeze(&self) {
        todo!()
    }

    pub fn vsplit(&self) {
        todo!()
    }

    pub fn vstack(&self, other: &Tensor<T>) -> Result<Tensor<T>, errors::ErrorKind> {
        self.atleast_2d().cat(&other.atleast_2d(), 0)
    }

    pub fn where_does(&self) {
        todo!()
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn reshape() {
        let t = Tensor::<i32>::arange(0, 27, 1)
            .unwrap()
            .reshape(vec![3, 3, 3])
            .unwrap();
        assert_eq!(*t.get_dim(), vec![3, 3, 3]);
        assert_eq!(*t.get_strides(), vec![9, 3, 1]);
        assert!(t.is_contiguous());

        let nt1 = t.get(vec![0..3, 2..3, 2..3]).unwrap();

        let nt2 = nt1.reshape(vec![3, 1]).unwrap();
        assert_eq!(*nt2.get_dim(), vec![3, 1]);
        assert_eq!(*nt2.get_strides(), vec![9, 1]);
        assert!(!nt2.is_contiguous());

        let nt3 = nt1.reshape(vec![3]).unwrap();
        assert_eq!(*nt3.get_dim(), vec![3]);
        assert_eq!(*nt3.get_strides(), vec![9]);
        assert!(!nt3.is_contiguous());

        let nt4 = nt1.reshape(vec![1, 3]).unwrap();
        assert_eq!(*nt4.get_dim(), vec![1, 3]);
        assert_eq!(*nt4.get_strides(), vec![9, 9]);
        assert!(!nt4.is_contiguous());

        let nt5 = nt1.reshape(vec![1, 1, 3]).unwrap();
        assert_eq!(*nt5.get_dim(), vec![1, 1, 3]);
        assert_eq!(*nt5.get_strides(), vec![9, 9, 9]);
        assert!(!nt5.is_contiguous());

        let nt6 = t.get(vec![0..2, 0..2, 0..2]).unwrap();

        let nt7 = nt6.reshape(vec![8]).unwrap();
        assert_eq!(*nt7.get_dim(), vec![8]);
        assert_eq!(*nt7.get_strides(), vec![1]);
        assert!(nt7.is_contiguous());

        let nt8 = nt6.reshape(vec![4, 2]).unwrap();
        assert_eq!(*nt8.get_dim(), vec![4, 2]);
        assert_eq!(*nt8.get_strides(), vec![2, 1]);

        let t2 = Tensor::<i8>::ones(vec![])
            .unwrap()
            .reshape(vec![1, 1, 1, 1])
            .unwrap();
        assert_eq!(*t2.get_dim(), vec![1, 1, 1, 1]);
        assert_eq!(*t2.get_strides(), vec![1, 1, 1, 1]);

        let nt9 = t2.reshape(vec![]).unwrap();
        assert_eq!(*nt9.get_dim(), vec![]);
        assert_eq!(*nt9.get_strides(), vec![]);

        let t3 = Tensor::<i128>::ones(vec![0, 2]).unwrap();
        let nt10 = t3.reshape(vec![4, 0]).unwrap();
        assert_eq!(*nt10.get_dim(), vec![4, 0]);
        assert_eq!(*nt10.get_strides(), vec![0, 1]);
    }

    #[test]
    fn reshape_invalid_parameters() {
        let t = Tensor::<i32>::arange(0, 27, 1)
            .unwrap()
            .reshape(vec![3, 3, 3])
            .unwrap();
        let t2 = t.reshape(vec![9, 4]);
        assert!(match t2 {
            Ok(_) => false,
            Err(e) => match e {
                errors::ErrorKind::InvalidParameter(_) => true,
                _ => false,
            },
        });

        let t3 = t.reshape(vec![9, 3, 1, 1, 1, 0]);
        assert!(match t3 {
            Ok(_) => false,
            Err(e) => match e {
                errors::ErrorKind::InvalidParameter(_) => true,
                _ => false,
            },
        });
    }

    #[test]
    fn argwhere() {
        let t = Tensor::<u128>::eye(3, 3).unwrap();
        t.upd(vec![0..1, 2..3], 1).unwrap();
        t.upd(vec![2..3, 0..1], 1).unwrap();
        t.upd(vec![2..3, 2..3], 1).unwrap();

        let nt = t.argwhere();
        assert_eq!(*nt.get_dim(), vec![5, 2]);
        assert_eq!(*nt.get_strides(), vec![2, 1]);
        assert_eq!(nt.contignious_vec(), vec![0, 0, 0, 2, 1, 1, 2, 0, 2, 2]);

        let t1 = Tensor::<f32>::ones(vec![3, 2, 2]).unwrap();
        t1.upd(vec![0..1, 0..1, 0..1], 0.3 - (0.2 + 0.1)).unwrap();
        t1.upd(vec![1..2, 0..1, 0..1], 0.3 - (0.2 + 0.1)).unwrap();
        t1.upd(vec![2..3, 0..1, 0..1], 0.3 - (0.2 + 0.1)).unwrap();

        let nt1 = t1.argwhere();
        assert_eq!(*nt1.get_dim(), vec![9, 3]);
        assert_eq!(*nt1.get_strides(), vec![3, 1]);
        assert_eq!(
            nt1.contignious_vec(),
            vec![0, 0, 1, 0, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 2, 0, 1, 2, 1, 0, 2, 1, 1]
        );

        let t2 = t.get(vec![1..3, 1..3]).unwrap();
        let nt2 = t2.argwhere();
        assert_eq!(*nt2.get_dim(), vec![2, 2]);
        assert_eq!(*nt2.get_strides(), vec![2, 1]);
        assert_eq!(nt2.contignious_vec(), vec![0, 0, 1, 1]);
    }

    #[test]
    fn cat() {
        let t1 = Tensor::<u16>::from_val(1, vec![3, 3])
            .unwrap()
            .cat(&Tensor::<u16>::from_val(2, vec![2, 3]).unwrap(), 0)
            .unwrap();
        assert_eq!(*t1.get_dim(), vec![5, 3]);
        assert_eq!(*t1.get_strides(), vec![3, 1]);
        assert_eq!(
            t1.contignious_vec(),
            vec![1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2]
        );
        assert!(t1.is_contiguous());

        let t2 = Tensor::<u16>::from_val(1, vec![3, 3])
            .unwrap()
            .cat(&Tensor::<u16>::from_val(2, vec![3, 2]).unwrap(), 1)
            .unwrap();
        println!("{:#?}", t2);
        assert_eq!(*t2.get_dim(), vec![3, 5]);
        assert_eq!(*t2.get_strides(), vec![5, 1]);
        assert_eq!(
            t2.contignious_vec(),
            vec![1, 1, 1, 2, 2, 1, 1, 1, 2, 2, 1, 1, 1, 2, 2]
        );
        assert!(t2.is_contiguous());

        let t3 = Tensor::<f64>::linspace(1.0, 30.0, 30)
            .unwrap()
            .reshape(vec![2, 3, 5])
            .unwrap()
            .to::<i64>()
            .unwrap();

        let t4 = Tensor::<f64>::linspace(101.0, 145.0, 45)
            .unwrap()
            .reshape(vec![3, 3, 5])
            .unwrap()
            .to::<i64>()
            .unwrap();
        let ct1 = t3.cat(&t4, 0).unwrap();
        assert_eq!(*ct1.get_dim(), vec![5, 3, 5]);
        assert_eq!(*ct1.get_strides(), vec![15, 5, 1]);
        assert_eq!(
            ct1.contignious_vec(),
            vec![
                1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23,
                24, 25, 26, 27, 28, 29, 30, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111,
                112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127,
                128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143,
                144, 145
            ]
        );
        assert!(ct1.is_contiguous());

        let t5 = Tensor::<f64>::linspace(101.0, 130.0, 30)
            .unwrap()
            .reshape(vec![2, 3, 5])
            .unwrap()
            .to::<i64>()
            .unwrap();
        let ct2 = t3.cat(&t5.shallow_copy(), 1).unwrap();
        assert_eq!(*ct2.get_dim(), vec![2, 6, 5]);
        assert_eq!(*ct2.get_strides(), vec![30, 5, 1]);
        assert_eq!(
            ct2.contignious_vec(),
            vec![
                1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 101, 102, 103, 104, 105, 106,
                107, 108, 109, 110, 111, 112, 113, 114, 115, 16, 17, 18, 19, 20, 21, 22, 23, 24,
                25, 26, 27, 28, 29, 30, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127,
                128, 129, 130
            ]
        );
        assert!(ct2.is_contiguous());

        let t6 = Tensor::<f64>::linspace(101.0, 124.0, 24)
            .unwrap()
            .reshape(vec![2, 3, 4])
            .unwrap()
            .to::<i64>()
            .unwrap();
        let ct3 = t3.cat(&t6, 2).unwrap();
        assert_eq!(*ct3.get_dim(), vec![2, 3, 9]);
        assert_eq!(*ct3.get_strides(), vec![27, 9, 1]);
        assert_eq!(
            ct3.contignious_vec(),
            vec![
                1, 2, 3, 4, 5, 101, 102, 103, 104, 6, 7, 8, 9, 10, 105, 106, 107, 108, 11, 12, 13,
                14, 15, 109, 110, 111, 112, 16, 17, 18, 19, 20, 113, 114, 115, 116, 21, 22, 23, 24,
                25, 117, 118, 119, 120, 26, 27, 28, 29, 30, 121, 122, 123, 124
            ]
        );
        assert!(ct3.is_contiguous());
    }

    #[test]
    fn cat_invalid_parameters() {
        let t1 = Tensor::<f64>::linspace(1.0, 30.0, 30)
            .unwrap()
            .reshape(vec![2, 3, 5])
            .unwrap()
            .to::<i64>()
            .unwrap();

        let t2 = Tensor::<f64>::linspace(101.0, 145.0, 45)
            .unwrap()
            .reshape(vec![3, 3, 5])
            .unwrap()
            .to::<i64>()
            .unwrap();

        let t3 = t1.cat(&t2.shallow_copy(), 1);
        assert!(match t3 {
            Ok(_) => false,
            Err(e) => match e {
                errors::ErrorKind::InvalidParameter(_) => true,
                _ => false,
            },
        });

        let t4 = t1.cat(&t2.shallow_copy(), 100);
        assert!(match t4 {
            Ok(_) => false,
            Err(e) => match e {
                errors::ErrorKind::InvalidParameter(_) => true,
                _ => false,
            },
        });

        let t5 = Tensor::<f64>::linspace(101.0, 190.0, 90)
            .unwrap()
            .reshape(vec![3, 3, 5, 2])
            .unwrap()
            .to::<i64>()
            .unwrap();

        let t6 = t1.cat(&t5.shallow_copy(), 1);
        assert!(match t6 {
            Ok(_) => false,
            Err(e) => match e {
                errors::ErrorKind::InvalidParameter(_) => true,
                _ => false,
            },
        });

        let t7 = Tensor::<i64>::from_val(1, vec![5, 0]).unwrap();

        let t8 = t1.cat(&t7.shallow_copy(), 1);
        assert!(match t8 {
            Ok(_) => false,
            Err(e) => match e {
                errors::ErrorKind::InvalidParameter(_) => true,
                _ => false,
            },
        });
    }

    #[test]
    fn tensor_split() {
        let t = Tensor::<i32>::arange(0, 27, 1).unwrap();
        let split_t_9 = t.tensor_split(9, 0).unwrap();
        split_t_9.iter().enumerate().for_each(|(idx, ten)| {
            let idx = idx as i32;
            assert_eq!(*ten.get_dim(), vec![3]);
            assert_eq!(*ten.get_strides(), vec![1]);
            assert_eq!(ten.get_offset(), (idx as u64) * 3);
            assert_eq!(
                ten.contignious_vec(),
                vec![idx * 3, idx * 3 + 1, idx * 3 + 2]
            );
            assert!(ten.is_contiguous());
        });

        let t1 = Tensor::<i32>::arange(0, 14, 1)
            .unwrap()
            .reshape(vec![2, 7])
            .unwrap();
        let split_t1_3 = t1.tensor_split(3, 1).unwrap();
        split_t1_3.iter().enumerate().for_each(|(idx, ten)| {
            if idx == 0 {
                assert_eq!(*ten.get_dim(), vec![2, 3]);
                assert_eq!(ten.get_offset(), 0);
            } else {
                assert_eq!(*ten.get_dim(), vec![2, 2]);
                assert_eq!(ten.get_offset(), if idx == 1 { 3 } else { 5 });
            }
            assert_eq!(*ten.get_strides(), vec![7, 1]);
            assert!(!ten.is_contiguous());
        });
    }

    #[test]
    fn tensor_split_invalid_parameters() {
        let t = Tensor::<i32>::arange(0, 27, 1).unwrap();

        let split_t_0 = t.tensor_split(0, 0);
        assert!(match split_t_0 {
            Ok(_) => false,
            Err(e) => match e {
                errors::ErrorKind::InvalidParameter(_) => true,
                _ => false,
            },
        });

        let split_t_28 = t.tensor_split(28, 0);
        assert!(match split_t_28 {
            Ok(_) => false,
            Err(e) => match e {
                errors::ErrorKind::InvalidParameter(_) => true,
                _ => false,
            },
        });

        let split_t_9 = t.tensor_split(9, 1);
        assert!(match split_t_9 {
            Ok(_) => false,
            Err(e) => match e {
                errors::ErrorKind::InvalidParameter(_) => true,
                _ => false,
            },
        });

        let t1 = Tensor::<u16>::from_val(1, vec![5, 0]).unwrap();
        let split_t1_2 = t1.tensor_split(2, 0);
        assert!(match split_t1_2 {
            Ok(_) => false,
            Err(e) => match e {
                errors::ErrorKind::InvalidParameter(_) => true,
                _ => false,
            },
        });
    }

    #[test]
    fn tensor_split_index() {
        let t = Tensor::<i32>::arange(0, 27, 1).unwrap();
        let split_t = t.tensor_split_index(vec![3, 16], 0).unwrap();
        assert_eq!(*split_t[0].get_dim(), vec![3]);
        assert_eq!(*split_t[0].get_strides(), vec![1]);
        assert_eq!(split_t[0].get_offset(), 0);
        assert_eq!(split_t[0].contignious_vec(), vec![0, 1, 2]);
        assert_eq!(*split_t[1].get_dim(), vec![13]);
        assert_eq!(*split_t[1].get_strides(), vec![1]);
        assert_eq!(split_t[1].get_offset(), 3);
        assert_eq!(
            split_t[1].contignious_vec(),
            vec![3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
        );
        assert_eq!(*split_t[2].get_dim(), vec![11]);
        assert_eq!(*split_t[2].get_strides(), vec![1]);
        assert_eq!(split_t[2].get_offset(), 16);
        assert_eq!(
            split_t[2].contignious_vec(),
            vec![16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26]
        );

        let t1 = Tensor::<i32>::arange(0, 14, 1)
            .unwrap()
            .reshape(vec![2, 7])
            .unwrap();
        let split_t1 = t1.tensor_split_index(vec![4], 1).unwrap();
        assert_eq!(*split_t1[0].get_dim(), vec![2, 4]);
        assert_eq!(*split_t1[0].get_strides(), vec![7, 1]);
        assert_eq!(split_t1[0].get_offset(), 0);
        assert_eq!(split_t1[0].contignious_vec(), vec![0, 1, 2, 3, 7, 8, 9, 10]);
        assert_eq!(*split_t1[1].get_dim(), vec![2, 3]);
        assert_eq!(*split_t1[1].get_strides(), vec![7, 1]);
        assert_eq!(split_t1[1].get_offset(), 4);
        assert_eq!(split_t1[1].contignious_vec(), vec![4, 5, 6, 11, 12, 13]);

        let t2 = Tensor::<i32>::arange(0, 12, 1)
            .unwrap()
            .reshape(vec![2, 2, 3])
            .unwrap();
        let split_t2 = t2.tensor_split_index(vec![], 2).unwrap();
        assert_eq!(*split_t2[0].get_dim(), vec![2, 2, 3]);
        assert_eq!(*split_t2[0].get_strides(), vec![6, 3, 1]);
        assert_eq!(split_t2[0].get_offset(), 0);
        assert_eq!(
            split_t2[0].contignious_vec(),
            vec![0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
        );
    }

    #[test]
    fn tensor_split_index_invalid_parameters() {
        let t = Tensor::<i32>::arange(0, 27, 1).unwrap();
        let split_t1 = t.tensor_split_index(vec![], 100);
        assert!(match split_t1 {
            Ok(_) => false,
            Err(e) => match e {
                errors::ErrorKind::InvalidParameter(_) => true,
                _ => false,
            },
        });

        let split_t2 = t.tensor_split_index(vec![10, 5, 17], 0);
        assert!(match split_t2 {
            Ok(_) => false,
            Err(e) => match e {
                errors::ErrorKind::InvalidParameter(_) => true,
                _ => false,
            },
        });

        let split_t3 = t.tensor_split_index(vec![10, 27], 0);
        assert!(match split_t3 {
            Ok(_) => false,
            Err(e) => match e {
                errors::ErrorKind::InvalidParameter(_) => true,
                _ => false,
            },
        });

        let t1 = Tensor::<u16>::from_val(1, vec![5, 0]).unwrap();
        let split_t1_2 = t1.tensor_split_index(vec![2], 0);
        assert!(match split_t1_2 {
            Ok(_) => false,
            Err(e) => match e {
                errors::ErrorKind::InvalidParameter(_) => true,
                _ => false,
            },
        });
    }

    #[test]
    fn dstack() {
        let a = Tensor::<i32>::arange(1, 4, 1).unwrap();
        let b = Tensor::<i32>::arange(4, 7, 1).unwrap();
        let c = a.dstack(&b.shallow_copy()).unwrap();
        assert_eq!(*c.get_dim(), vec![3, 1, 2]);
        assert_eq!(*c.get_strides(), vec![2, 2, 1]);
        assert_eq!(c.contignious_vec(), vec![1, 4, 2, 5, 3, 6]);

        let x = a.atleast_2d();
        let y = b.atleast_2d();
        let z = x.dstack(&y.shallow_copy()).unwrap();
        assert_eq!(*z.get_dim(), vec![3, 1, 2]);
        assert_eq!(*z.get_strides(), vec![2, 2, 1]);
        assert_eq!(z.contignious_vec(), vec![1, 4, 2, 5, 3, 6]);
    }

    #[test]
    fn permute() {
        let t = Tensor::<i32>::arange(0, 27, 1)
            .unwrap()
            .reshape(vec![3, 3, 3])
            .unwrap();
        let pt1 = t
            .get(vec![0..3, 0..2, 0..3])
            .unwrap()
            .permute(vec![0, 2, 1])
            .unwrap();
        assert_eq!(*pt1.get_dim(), vec![3, 3, 2]);
        assert_eq!(*pt1.get_strides(), vec![9, 1, 3]);
        assert_eq!(
            pt1.contignious_vec(),
            vec![0, 3, 1, 4, 2, 5, 9, 12, 10, 13, 11, 14, 18, 21, 19, 22, 20, 23]
        );

        let t1 = Tensor::<i32>::from_val(1, vec![]).unwrap();
        let pt2 = t1.permute(vec![]).unwrap();
        assert_eq!(*pt2.get_dim(), vec![]);
        assert_eq!(*pt2.get_strides(), vec![]);
        assert_eq!(pt2.contignious_vec(), vec![1]);

        let pt3 = Tensor::<i32>::from_val(1, vec![5, 6, 0])
            .unwrap()
            .permute(vec![0, 2, 1])
            .unwrap();
        assert_eq!(*pt3.get_dim(), vec![5, 0, 6]);
        assert_eq!(*pt3.get_strides(), vec![0, 1, 0]);
        assert_eq!(pt3.contignious_vec(), vec![]);
    }

    #[test]
    fn permute_invalid_parameters() {
        let t = Tensor::<i32>::arange(0, 27, 1)
            .unwrap()
            .reshape(vec![3, 3, 3])
            .unwrap();

        let pt1 = t
            .get(vec![0..3, 0..2, 0..3])
            .unwrap()
            .permute(vec![0, 2, 1, 3]);
        assert!(match pt1 {
            Ok(_) => false,
            Err(e) => match e {
                errors::ErrorKind::InvalidParameter(_) => true,
                _ => false,
            },
        });

        let pt2 = t
            .get(vec![0..3, 0..2, 0..3])
            .unwrap()
            .permute(vec![0, 3, 1]);
        assert!(match pt2 {
            Ok(_) => false,
            Err(e) => match e {
                errors::ErrorKind::InvalidParameter(_) => true,
                _ => false,
            },
        });

        let pt3 = t
            .get(vec![0..3, 0..2, 0..3])
            .unwrap()
            .permute(vec![0, 1, 1]);
        assert!(match pt3 {
            Ok(_) => false,
            Err(e) => match e {
                errors::ErrorKind::InvalidParameter(_) => true,
                _ => false,
            },
        });
    }

    #[test]
    fn transpose() {
        let t = Tensor::<i32>::arange(0, 18, 1)
            .unwrap()
            .reshape(vec![3, 2, 3])
            .unwrap();
        let tt1 = t.transpose(1, 2).unwrap();
        assert_eq!(*tt1.get_dim(), vec![3, 3, 2]);
        assert_eq!(*tt1.get_strides(), vec![6, 1, 3]);
        assert_eq!(
            tt1.contignious_vec(),
            vec![0, 3, 1, 4, 2, 5, 6, 9, 7, 10, 8, 11, 12, 15, 13, 16, 14, 17]
        );

        let t2 = Tensor::<i32>::arange(0, 9, 1)
            .unwrap()
            .reshape(vec![3, 3])
            .unwrap();
        let tt2 = t2.transpose(0, 1).unwrap();
        assert_eq!(*tt2.get_dim(), vec![3, 3]);
        assert_eq!(*tt2.get_strides(), vec![1, 3]);
        assert_eq!(tt2.contignious_vec(), vec![0, 3, 6, 1, 4, 7, 2, 5, 8]);
    }

    #[test]
    fn transpose_invalid_parameters() {
        let t = Tensor::<i32>::arange(0, 18, 1)
            .unwrap()
            .reshape(vec![3, 2, 3])
            .unwrap();
        let tt1 = t.transpose(1, 3);
        assert!(match tt1 {
            Ok(_) => false,
            Err(e) => match e {
                errors::ErrorKind::InvalidParameter(_) => true,
                _ => false,
            },
        });

        let t2 = Tensor::<i32>::arange(0, 9, 1)
            .unwrap()
            .reshape(vec![3, 3])
            .unwrap();
        let tt2 = t2.transpose(2, 1);
        assert!(match tt2 {
            Ok(_) => false,
            Err(e) => match e {
                errors::ErrorKind::InvalidParameter(_) => true,
                _ => false,
            },
        });
    }

    #[test]
    fn movedim() {
        let t = Tensor::<i32>::arange(0, 6, 1)
            .unwrap()
            .reshape(vec![3, 2, 1])
            .unwrap();
        let mt1 = t.movedim(0, 2).unwrap();
        assert_eq!(*mt1.get_dim(), vec![2, 1, 3]);
        assert_eq!(*mt1.get_strides(), vec![1, 1, 2]);
        assert_eq!(mt1.contignious_vec(), vec![0, 2, 4, 1, 3, 5]);
    }

    #[test]
    fn narrow() {
        let t = Tensor::<i32>::arange(1, 10, 1)
            .unwrap()
            .reshape(vec![3, 3])
            .unwrap();

        let nt1 = t.narrow(0, 0, 2).unwrap();
        assert_eq!(*nt1.get_dim(), vec![2, 3]);
        assert_eq!(*nt1.get_strides(), vec![3, 1]);
        assert_eq!(nt1.contignious_vec(), vec![1, 2, 3, 4, 5, 6]);
        assert!(nt1.is_contiguous());

        let nt2 = t.narrow(1, 1, 2).unwrap();
        assert_eq!(*nt2.get_dim(), vec![3, 2]);
        assert_eq!(*nt2.get_strides(), vec![3, 1]);
        assert_eq!(nt2.contignious_vec(), vec![2, 3, 5, 6, 8, 9]);
        assert!(!nt2.is_contiguous());
    }

    #[test]
    fn narrow_invalid_parameters() {
        let t = Tensor::<i32>::arange(1, 10, 1)
            .unwrap()
            .reshape(vec![3, 3])
            .unwrap();

        let nt1 = t.narrow(10, 0, 2);
        assert!(match nt1 {
            Ok(_) => false,
            Err(e) => match e {
                errors::ErrorKind::InvalidParameter(_) => true,
                _ => false,
            },
        });

        let nt2 = t.narrow(1, 1, 3);
        assert!(match nt2 {
            Ok(_) => false,
            Err(e) => match e {
                errors::ErrorKind::InvalidParameter(_) => true,
                _ => false,
            },
        });

        let nt3 = t.narrow(1, 4, 1);
        assert!(match nt3 {
            Ok(_) => false,
            Err(e) => match e {
                errors::ErrorKind::InvalidParameter(_) => true,
                _ => false,
            },
        });
    }

    #[test]
    fn nonzero() {
        let t1 = Tensor::<i32>::from_val(1, vec![5]).unwrap();
        t1.upd(vec![3..4], 0).unwrap();

        let nzt1 = t1.nonzero().unwrap();
        assert_eq!(nzt1.len(), 1);
        assert_eq!(*nzt1[0].get_dim(), vec![4]);
        assert_eq!(*nzt1[0].get_strides(), vec![1]);
        assert_eq!(nzt1[0].contignious_vec(), vec![0, 1, 2, 4]);

        let t2 = Tensor::<f32>::eye(4, 4).unwrap();
        t2.upd(vec![0..1, 0..1], 0.6).unwrap();
        t2.upd(vec![1..2, 1..2], 0.4).unwrap();
        t2.upd(vec![2..3, 2..3], 1.2).unwrap();
        t2.upd(vec![3..4, 3..4], -0.4).unwrap();

        let nzt2 = t2.nonzero().unwrap();
        assert_eq!(nzt2.len(), 2);
        nzt2.iter().for_each(|ten| {
            assert_eq!(*ten.get_dim(), vec![4]);
            assert_eq!(*ten.get_strides(), vec![2]);
            assert_eq!(ten.contignious_vec(), vec![0, 1, 2, 3]);
        })
    }
}
