use std::ops::{self, Range};

use num::{Bounded, NumCast, ToPrimitive};

use crate::errors;

use super::{storage::StorageTrait, utils, Tensor};

// -------------------------- Helper Methods --------------------------
impl<T: Bounded + NumCast + ToPrimitive + Copy + ops::AddAssign + PartialOrd> Tensor<T> {
    pub(super) fn contignious_storage_index_from_dim_index(
        &self,
        dim_index: &Vec<u64>,
    ) -> Result<u64, errors::ErrorKind> {
        if self.get_dim().is_empty() {
            return Err(errors::ErrorKind::InvalidParameter(
                "Expected a >= 1 dimensional tensor".to_string(),
            ));
        }
        if dim_index.len() != self.no_dim() as usize
            || dim_index
                .iter()
                .zip(self.get_dim().iter())
                .any(|(x, y)| x >= y)
        {
            return Err(errors::ErrorKind::InvalidParameter(
                "Index out of Bounds".to_string(),
            ));
        }

        Ok(self.contignious_storage_index_from_dim_index_unchecked(dim_index))
    }

    pub(super) fn contignious_storage_index_from_dim_index_unchecked(
        &self,
        dim_index: &Vec<u64>,
    ) -> u64 {
        self.get_offset()
            + dim_index
                .iter()
                .zip(self.get_strides().iter())
                .map(|(x, y)| x * y)
                .sum::<u64>()
    }

    pub(super) fn dim_index_from_index(&self, idx: u64) -> Result<Vec<u64>, errors::ErrorKind> {
        if idx >= self.num_el() {
            return Err(errors::ErrorKind::InvalidParameter(
                format!(
                    "Idx out of bounds, expected: < {}, got: {}",
                    self.num_el(),
                    idx
                )
                .to_string(),
            ));
        }

        Ok(self.dim_index_from_index_unchecked(idx))
    }

    pub(super) fn dim_index_from_index_unchecked(&self, idx: u64) -> Vec<u64> {
        utils::strides_from_dimensions(self.get_dim().clone())
            .iter()
            .scan(idx, |state, stride| {
                let ret = *state / stride;
                *state %= stride;
                Some(ret)
            })
            .collect()
    }

    pub(super) fn contignious_vec(&self) -> Vec<T> {
        let storage_borrow = self.get_storage_ptr().borrow();
        let storage_vec = storage_borrow.get_vec();
        if self.no_dim() == 0 {
            return vec![storage_vec[self.get_offset() as usize]];
        }
        if self.get_dim().contains(&0) {
            return vec![];
        }

        let mut ans = vec![];
        let mut stack = vec![0; self.no_dim() as usize];
        while !stack.is_empty() {
            ans.push(
                storage_vec
                    [self.contignious_storage_index_from_dim_index_unchecked(&stack) as usize],
            );

            while !stack.is_empty() && *stack.last().unwrap() == self.get_dim()[stack.len() - 1] - 1
            {
                stack.pop();
            }

            if !stack.is_empty() {
                *stack.last_mut().unwrap() += 1;
                while stack.len() < self.no_dim() as usize {
                    stack.push(0);
                }
            }
        }

        ans
    }

    pub(super) fn indices_of_subtensor(
        &self,
        rngs: Vec<Range<u64>>,
    ) -> Result<Vec<u64>, errors::ErrorKind> {
        if rngs.len() != self.no_dim() as usize {
            return Err(errors::ErrorKind::InvalidParameter(
                format!(
                    "Unexpected Number of Indicies given, expected: {}, got: {}",
                    self.no_dim(),
                    rngs.len()
                )
                .to_string(),
            ));
        }
        if rngs
            .iter()
            .zip(self.get_dim().iter())
            .any(|(rng, dim)| (rng.start >= *dim) || (0u64 >= rng.end) || (rng.end > *dim))
        {
            return Err(errors::ErrorKind::InvalidParameter(
                "Index out of Bounds".to_string(),
            ));
        }

        if rngs.iter().any(|rng| rng.is_empty()) {
            return Err(errors::ErrorKind::InvalidParameter(
                "Expected Index range to be non empty".to_string(),
            ));
        }
        Ok(self.indices_of_subtensor_unchecked(rngs))
    }

    pub(super) fn indices_of_subtensor_unchecked(&self, rngs: Vec<Range<u64>>) -> Vec<u64> {
        let mut res = vec![];
        fn dfs(
            dim_no: u64,
            curr_contig_idx: u64,
            rngs: &Vec<Range<u64>>,
            strides: &Vec<u64>,
            res: &mut Vec<u64>,
        ) {
            if dim_no == rngs.len() as u64 {
                res.push(curr_contig_idx);
                return;
            }

            for val in rngs[dim_no as usize].clone() {
                dfs(
                    dim_no + 1,
                    curr_contig_idx + val * strides[dim_no as usize],
                    rngs,
                    strides,
                    res,
                );
            }
        }
        dfs(0, 0, &rngs, &self.get_strides(), &mut res);
        return res;
    }

    // TODO: Tests
    pub(super) fn get_at_dim_index(&self, dim_index: &Vec<u64>) -> Result<T, errors::ErrorKind> {
        if dim_index
            .iter()
            .zip(self.get_dim().iter())
            .any(|(idx, dim_sz)| idx >= dim_sz)
        {
            return Err(errors::ErrorKind::InvalidParameter(
                "Dim Index out of range".to_string(),
            ));
        }

        Ok(self.get_at_dim_index_unchecked(dim_index))
    }

    // TODO: Tests
    pub(super) fn get_at_dim_index_unchecked(&self, dim_index: &Vec<u64>) -> T {
        let contig_idx = self.contignious_storage_index_from_dim_index_unchecked(dim_index);
        *self.get_storage_ptr().borrow().at_vec(contig_idx)
    }

    // TODO: Tests
    pub(super) fn upd_at_dim_index(
        &self,
        dim_index: &Vec<u64>,
        val: T,
    ) -> Result<(), errors::ErrorKind> {
        if dim_index
            .iter()
            .zip(self.get_dim().iter())
            .any(|(idx, dim_sz)| idx >= dim_sz)
        {
            return Err(errors::ErrorKind::InvalidParameter(
                "Dim Index out of range".to_string(),
            ));
        }

        self.upd_at_dim_index_unchecked(dim_index, val);
        Ok(())
    }

    // TODO: Tests
    pub(super) fn upd_at_dim_index_unchecked(&self, dim_index: &Vec<u64>, val: T) {
        let contig_idx = self.contignious_storage_index_from_dim_index_unchecked(dim_index);
        self.get_storage_ptr().borrow_mut().upd_vec(contig_idx, val);
    }

    // TODO: Tests
    pub(super) fn get_at_contig_index(&self, contig_idx: u64) -> Result<T, errors::ErrorKind> {
        if contig_idx >= self.get_storage_ptr().borrow().len() {
            return Err(errors::ErrorKind::InvalidParameter(
                "Contiguous Index out of range".to_string(),
            ));
        }

        Ok(self.get_at_contig_index_unchecked(contig_idx))
    }

    // TODO: Tests
    pub(super) fn get_at_contig_index_unchecked(&self, contig_idx: u64) -> T {
        *self.get_storage_ptr().borrow().at_vec(contig_idx)
    }

    // TODO: Tests
    pub(super) fn upd_at_contig_index(
        &self,
        contig_idx: u64,
        val: T,
    ) -> Result<(), errors::ErrorKind> {
        if contig_idx >= self.get_storage_ptr().borrow().len() {
            return Err(errors::ErrorKind::InvalidParameter(
                "Contiguous Index out of range".to_string(),
            ));
        }

        self.upd_at_contig_index_unchecked(contig_idx, val);
        Ok(())
    }

    // TODO: Tests
    pub(super) fn upd_at_contig_index_unchecked(&self, contig_idx: u64, val: T) {
        self.get_storage_ptr().borrow_mut().upd_vec(contig_idx, val);
    }

    pub(super) fn atleast_1d(&self) -> Tensor<T> {
        let res = self.shallow_copy();
        if res.no_dim() == 0 {
            return res.reshape_unchecked(vec![1]);
        }
        res
    }

    pub(super) fn atleast_2d(&self) -> Tensor<T> {
        let res = self.shallow_copy().atleast_1d();
        if res.no_dim() == 1 {
            return res.reshape_unchecked(vec![res.get_dim()[0], 1]);
        }
        res
    }

    pub(super) fn atleast_3d(&self) -> Tensor<T> {
        let res = self.shallow_copy().atleast_2d();
        if res.no_dim() == 2 {
            let res_dim = res.get_dim();
            return res.reshape_unchecked(vec![res_dim[0], res_dim[1], 1]);
        }
        res
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn contignious_index_from_dim_index() {
        let zeros = Tensor::<i32>::zeros(vec![2, 2, 2]).unwrap();
        assert_eq!(
            zeros
                .contignious_storage_index_from_dim_index(&vec![0u64, 0u64, 0u64])
                .unwrap(),
            0u64
        );
        assert_eq!(
            zeros
                .contignious_storage_index_from_dim_index(&vec![0u64, 0u64, 1u64])
                .unwrap(),
            1u64
        );
        assert_eq!(
            zeros
                .contignious_storage_index_from_dim_index(&vec![0u64, 1u64, 0u64])
                .unwrap(),
            2u64
        );
        assert_eq!(
            zeros
                .contignious_storage_index_from_dim_index(&vec![0u64, 1u64, 1u64])
                .unwrap(),
            3u64
        );
        assert_eq!(
            zeros
                .contignious_storage_index_from_dim_index(&vec![1u64, 0u64, 0u64])
                .unwrap(),
            4u64
        );
        assert_eq!(
            zeros
                .contignious_storage_index_from_dim_index(&vec![1u64, 0u64, 1u64])
                .unwrap(),
            5u64
        );
        assert_eq!(
            zeros
                .contignious_storage_index_from_dim_index(&vec![1u64, 1u64, 0u64])
                .unwrap(),
            6u64
        );
        assert_eq!(
            zeros
                .contignious_storage_index_from_dim_index(&vec![1u64, 1u64, 1u64])
                .unwrap(),
            7u64
        );

        let ones = Tensor::<i128>::ones(vec![2, 3, 5]).unwrap();
        assert_eq!(
            ones.contignious_storage_index_from_dim_index(&vec![0u64, 1u64, 3u64])
                .unwrap(),
            8
        );
        assert_eq!(
            ones.contignious_storage_index_from_dim_index(&vec![0u64, 2u64, 2u64])
                .unwrap(),
            12
        );
        assert_eq!(
            ones.contignious_storage_index_from_dim_index(&vec![1u64, 2u64, 4u64])
                .unwrap(),
            29
        );
    }

    #[test]
    fn contignious_index_from_dim_index_invald_parameters() {
        let ones = Tensor::<i128>::ones(vec![2, 3, 5]).unwrap();
        let csi1 = ones.contignious_storage_index_from_dim_index(&vec![1u64, 2u64, 5u64]);
        assert!(match csi1 {
            Ok(_) => false,
            Err(e) => match e {
                errors::ErrorKind::InvalidParameter(_) => true,
                _ => false,
            },
        });

        let zeros = Tensor::<i128>::zeros(vec![7, 3, 9]).unwrap();
        let csi0 = zeros.contignious_storage_index_from_dim_index(&vec![27u64, 2u64, 5u64]);
        assert!(match csi0 {
            Ok(_) => false,
            Err(e) => match e {
                errors::ErrorKind::InvalidParameter(_) => true,
                _ => false,
            },
        });

        let zero_dimensional_tensor = Tensor::<f64>::from_val(3.141592, vec![]).unwrap();
        let csi_pi = zero_dimensional_tensor.contignious_storage_index_from_dim_index(&vec![]);
        assert!(match csi_pi {
            Ok(_) => false,
            Err(e) => match e {
                errors::ErrorKind::InvalidParameter(_) => true,
                _ => false,
            },
        });
    }

    #[test]
    fn dim_index_contignious_index() {
        let zeros = Tensor::<i32>::zeros(vec![2, 2, 2]).unwrap();
        assert_eq!(
            zeros.dim_index_from_index(0u64).unwrap(),
            vec![0u64, 0u64, 0u64]
        );
        assert_eq!(
            zeros.dim_index_from_index(1u64).unwrap(),
            vec![0u64, 0u64, 1u64]
        );
        assert_eq!(
            zeros.dim_index_from_index(2u64).unwrap(),
            vec![0u64, 1u64, 0u64]
        );
        assert_eq!(
            zeros.dim_index_from_index(3u64).unwrap(),
            vec![0u64, 1u64, 1u64]
        );
        assert_eq!(
            zeros.dim_index_from_index(4u64).unwrap(),
            vec![1u64, 0u64, 0u64]
        );
        assert_eq!(
            zeros.dim_index_from_index(5u64).unwrap(),
            vec![1u64, 0u64, 1u64]
        );
        assert_eq!(
            zeros.dim_index_from_index(6u64).unwrap(),
            vec![1u64, 1u64, 0u64]
        );
        assert_eq!(
            zeros.dim_index_from_index(7u64).unwrap(),
            vec![1u64, 1u64, 1u64]
        );

        let ones = Tensor::<i128>::ones(vec![2, 3, 5]).unwrap();
        assert_eq!(
            ones.dim_index_from_index(8).unwrap(),
            vec![0u64, 1u64, 3u64]
        );
        assert_eq!(
            ones.dim_index_from_index(12).unwrap(),
            vec![0u64, 2u64, 2u64]
        );
        assert_eq!(
            ones.dim_index_from_index(29).unwrap(),
            vec![1u64, 2u64, 4u64]
        );
    }

    #[test]
    fn contignious_vec() {
        let t = Tensor::<i32>::arange(0, 27, 1)
            .unwrap()
            .reshape(vec![3, 3, 3])
            .unwrap();
        assert_eq!(t.contignious_vec(), (0..27).collect::<Vec<i32>>());

        let t2 = Tensor::<i16>::eye(3, 4).unwrap();
        assert_eq!(
            t2.contignious_vec(),
            vec![1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0]
        );
    }

    #[test]
    fn indices_of_subtensor() {
        let t = Tensor::<i32>::arange(0, 27, 1)
            .unwrap()
            .reshape(vec![3, 3, 3])
            .unwrap();

        let nt1 = t.indices_of_subtensor(vec![0..3, 2..3, 2..3]).unwrap();
        assert_eq!(nt1, vec![8, 17, 26]);

        let nt2 = t.indices_of_subtensor(vec![0..2, 0..2, 0..2]).unwrap();
        assert_eq!(nt2, vec![0, 1, 3, 4, 9, 10, 12, 13]);

        let nt3 = t.indices_of_subtensor(vec![1..2, 1..2, 1..2]).unwrap();
        assert_eq!(nt3, vec![13]);
    }
}
