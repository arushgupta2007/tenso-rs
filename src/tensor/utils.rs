use num::{NumCast, ToPrimitive};

use crate::{errors, types};

pub fn strides_from_dimensions(dim: Vec<u64>) -> Vec<u64> {
    let mut strides: Vec<u64> = dim
        .iter()
        .rev()
        .scan(1, |state, x| {
            let ret_val = *state;
            *state = *state * x;
            Some(ret_val as u64)
        })
        .collect();
    strides.reverse();
    return strides;
}

pub fn dim_index_from_index(idx: u64, dims: Vec<u64>) -> Result<Vec<u64>, errors::ErrorKind> {
    let num_el = dims.iter().product();
    if idx >= num_el {
        return Err(errors::ErrorKind::InvalidParameter(
            format!("Idx out of bounds, expected: < {}, got: {}", num_el, idx).to_string(),
        ));
    }

    Ok(strides_from_dimensions(dims)
        .iter()
        .scan(idx, |state, stride| {
            let ret = *state / stride;
            *state %= stride;
            Some(ret)
        })
        .collect())
}

pub fn eq<T: NumCast + ToPrimitive + PartialEq>(a: T, b: T, dtype: types::Types) -> bool {
    match dtype {
        types::Types::Float32 => {
            let a: f32 = NumCast::from(a).unwrap();
            let b: f32 = NumCast::from(b).unwrap();
            (a - b).abs() <= f32::EPSILON
        }
        types::Types::Float64 => {
            let a: f64 = NumCast::from(a).unwrap();
            let b: f64 = NumCast::from(b).unwrap();
            (a - b).abs() <= f64::EPSILON
        }
        _ => a == b,
    }
}
