use std::{
    ops::{self, Range},
    rc::Rc,
};

use num::{Bounded, NumCast, ToPrimitive};

use crate::errors;

use super::{storage::StorageTrait, Tensor};

impl<T: Bounded + NumCast + ToPrimitive + Copy + ops::AddAssign + PartialOrd> Tensor<T> {
    pub fn get(&self, rngs: Vec<Range<u64>>) -> Result<Tensor<T>, errors::ErrorKind> {
        if rngs.len() != self.no_dim() as usize {
            return Err(errors::ErrorKind::InvalidParameter(
                format!(
                    "Unexpected Number of Indicies given, expected: {}, got: {}",
                    self.no_dim(),
                    rngs.len()
                )
                .to_string(),
            ));
        }
        if rngs
            .iter()
            .zip(self.get_dim().iter())
            .any(|(rng, dim)| (rng.start >= *dim) || (0u64 >= rng.end) || (rng.end > *dim))
        {
            return Err(errors::ErrorKind::InvalidParameter(
                "Index out of Bounds".to_string(),
            ));
        }

        if rngs.iter().any(|rng| rng.is_empty()) {
            return Err(errors::ErrorKind::InvalidParameter(
                "Expected Index range to be non empty".to_string(),
            ));
        }

        let new_storage = Rc::clone(&self.get_storage_ptr());
        let new_dim: Vec<u64> = rngs.iter().map(|rng| rng.end - rng.start).collect();
        let new_offset: u64 = rngs
            .iter()
            .zip(self.get_strides().iter())
            .map(|(rng, stride)| rng.start * stride)
            .sum();

        Ok(Tensor::<T>::from_raw(
            new_storage,
            new_dim,
            self.get_dtype(),
            self.get_strides().clone(),
            new_offset,
        ))
    }

    // TODO: Write Tests
    pub fn upd(&self, rngs: Vec<Range<u64>>, val: T) -> Result<(), errors::ErrorKind> {
        if rngs.len() != self.no_dim() as usize {
            return Err(errors::ErrorKind::InvalidParameter(
                format!(
                    "Unexpected Number of Indicies given, expected: {}, got: {}",
                    self.no_dim(),
                    rngs.len()
                )
                .to_string(),
            ));
        }
        if rngs
            .iter()
            .zip(self.get_dim().iter())
            .any(|(rng, dim)| (rng.start >= *dim) || (0u64 >= rng.end) || (rng.end > *dim))
        {
            return Err(errors::ErrorKind::InvalidParameter(
                "Index out of Bounds".to_string(),
            ));
        }

        if rngs.iter().any(|rng| rng.is_empty()) {
            return Err(errors::ErrorKind::InvalidParameter(
                "Expected Index range to be non empty".to_string(),
            ));
        }

        let mut storage_mut = self.get_storage_ptr().borrow_mut();
        self.indices_of_subtensor(rngs)?.iter().for_each(|x| {
            storage_mut.upd_vec(*x, val);
        });
        Ok(())
    }

    pub fn val(&self) -> Result<T, errors::ErrorKind> {
        if self.no_dim() != 0 {
            Err(errors::ErrorKind::InvalidParameter(
                format!(
                    "Invalid Tensor Dimension, expected: 0, got: {}",
                    self.no_dim()
                )
                .to_string(),
            ))
        } else {
            Ok(self.val_unchecked())
        }
    }

    pub(super) fn val_unchecked(&self) -> T {
        self.get_storage_ptr().borrow().get_vec()[0]
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn get() {
        let t = Tensor::<i32>::arange(0, 27, 1)
            .unwrap()
            .reshape(vec![3, 3, 3])
            .unwrap();

        let nt1 = t.get(vec![0..3, 2..3, 2..3]).unwrap();
        assert_eq!(nt1.contignious_vec(), vec![8, 17, 26]);
        assert_eq!(*nt1.get_dim(), vec![3, 1, 1]);
        assert_eq!(*nt1.get_strides(), vec![9, 3, 1]);
        assert_eq!(nt1.get_offset(), 8);

        let nt2 = t.get(vec![0..2, 0..2, 0..2]).unwrap();
        assert_eq!(nt2.contignious_vec(), vec![0, 1, 3, 4, 9, 10, 12, 13]);
        assert_eq!(*nt2.get_dim(), vec![2, 2, 2]);
        assert_eq!(*nt2.get_strides(), vec![9, 3, 1]);
        assert_eq!(nt2.get_offset(), 0);

        let nt3 = t.get(vec![1..2, 1..2, 1..2]).unwrap();
        assert_eq!(nt3.contignious_vec(), vec![13]);
        assert_eq!(*nt3.get_dim(), vec![1, 1, 1]);
        assert_eq!(*nt3.get_strides(), vec![9, 3, 1]);
        assert_eq!(nt3.get_offset(), 13);

        let nt4 = nt2.get(vec![0..2, 0..2, 1..2]).unwrap();
        assert_eq!(nt4.contignious_vec(), vec![1, 4, 10, 13]);
        assert_eq!(*nt4.get_dim(), vec![2, 2, 1]);
        assert_eq!(*nt4.get_strides(), vec![9, 3, 1]);
        assert_eq!(nt4.get_offset(), 1);

        t.get_storage_ptr().borrow_mut().upd_vec(4, 100);
        assert_eq!(nt2.get_storage_ptr().borrow().get_vec()[4], 100);
        assert_eq!(nt3.get_storage_ptr().borrow().get_vec()[4], 100);
        assert_eq!(nt4.get_storage_ptr().borrow().get_vec()[4], 100);
    }

    #[test]
    fn get_invalid_parameters() {
        let t = Tensor::<i32>::arange(0, 27, 1)
            .unwrap()
            .reshape(vec![3, 3, 3])
            .unwrap();

        let nt1 = t.get(vec![0..4, 2..3, 2..3]);
        assert!(match nt1 {
            Ok(_) => false,
            Err(e) => match e {
                errors::ErrorKind::InvalidParameter(_) => true,
                _ => false,
            },
        });

        let nt2 = t.get(vec![0..3, 3..3, 2..3]);
        assert!(match nt2 {
            Ok(_) => false,
            Err(e) => match e {
                errors::ErrorKind::InvalidParameter(_) => true,
                _ => false,
            },
        });

        let nt3 = t.get(vec![0..3, 2..3]);
        assert!(match nt3 {
            Ok(_) => false,
            Err(e) => match e {
                errors::ErrorKind::InvalidParameter(_) => true,
                _ => false,
            },
        });
    }
}
