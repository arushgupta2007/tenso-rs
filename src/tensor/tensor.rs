use std::{cell::RefCell, fmt::Debug, fmt::Display, ops, rc::Rc};

use num::{Bounded, NumCast, ToPrimitive};

use crate::{errors, types};

use super::{
    storage::{self, StorageTrait},
    utils,
};

pub struct Tensor<T: ToPrimitive> {
    storage: Rc<RefCell<storage::Storage<T>>>,
    dim: Vec<u64>,
    dtype: types::Types,
    strides: Vec<u64>,
    offset: u64,
}

impl<T: Bounded + NumCast + ToPrimitive + Copy + PartialOrd + ops::AddAssign> Tensor<T> {
    pub(super) fn from_raw(
        storage: Rc<RefCell<storage::Storage<T>>>,
        dim: Vec<u64>,
        dtype: types::Types,
        strides: Vec<u64>,
        offset: u64,
    ) -> Tensor<T> {
        Tensor {
            storage,
            dim,
            dtype,
            strides,
            offset,
        }
    }

    pub fn get_dtype(&self) -> types::Types {
        self.dtype
    }
    pub fn get_dim(&self) -> &Vec<u64> {
        &self.dim
    }
    pub fn get_strides(&self) -> &Vec<u64> {
        &self.strides
    }
    pub fn get_offset(&self) -> u64 {
        self.offset
    }
    pub fn get_storage_ptr(&self) -> &Rc<RefCell<storage::Storage<T>>> {
        &self.storage
    }

    pub fn no_dim(&self) -> u64 {
        self.get_dim().len() as u64
    }

    pub fn is_floating_point(&self) -> bool {
        match self.get_dtype() {
            types::Types::Float32 | types::Types::Float64 => true,
            _ => false,
        }
    }
    pub fn is_signed(&self) -> bool {
        match self.get_dtype() {
            types::Types::Uint8
            | types::Types::Uint16
            | types::Types::Uint32
            | types::Types::Uint64
            | types::Types::Uint128 => false,
            _ => true,
        }
    }
    pub fn is_contiguous(&self) -> bool {
        if self.no_dim() == 0 {
            return true;
        }

        return utils::strides_from_dimensions(self.get_dim().clone()) == *self.get_strides();
    }

    pub fn to_contiguous(&self) -> Tensor<T> {
        let storage_val = self.contignious_vec();
        let storage = Rc::new(RefCell::new(storage::Storage::new(storage_val)));

        Tensor::<T>::from_raw(
            storage,
            self.get_dim().clone(),
            self.get_dtype(),
            utils::strides_from_dimensions(self.get_dim().clone()),
            0,
        )
    }

    pub fn num_el(&self) -> u64 {
        self.get_dim().iter().product()
    }

    pub fn shallow_copy(&self) -> Tensor<T> {
        Tensor::<T>::from_raw(
            Rc::clone(self.get_storage_ptr()),
            self.get_dim().clone(),
            self.get_dtype(),
            self.get_strides().clone(),
            self.get_offset(),
        )
    }

    pub fn deep_copy(&self) -> Tensor<T> {
        self.to_contiguous()
    }
}

impl<T: Bounded + NumCast + ToPrimitive + Copy + ops::AddAssign + PartialOrd + Display> Debug
    for Tensor<T>
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if self.get_dim().contains(&0) {
            return write!(
                f,
                "{}",
                format!(
                    "Tensor (, dtype={}, dim={:?}, strides={:?}, offset={})",
                    self.get_dtype(),
                    self.get_dim(),
                    self.get_strides(),
                    self.get_offset()
                )
            );
        }

        if self.no_dim() == 0 {
            let element = self.val_unchecked();
            let res = format!(
                "Tensor ({}, dtype={}, dim={:?}, strides={:?}, offset={})",
                element,
                self.get_dtype(),
                self.get_dim(),
                self.get_strides(),
                self.get_offset()
            );
            return write!(f, "{}", res);
        }

        let mut element_str = String::new();
        for _ in 0..self.no_dim() {
            element_str.push('[');
        }

        let storage = self.get_storage_ptr().borrow();
        let storage_vec = storage.get_vec();
        let mut stack = vec![0; self.no_dim() as usize];
        while !stack.is_empty() {
            element_str += &format!(
                "{}",
                storage_vec
                    [self.contignious_storage_index_from_dim_index_unchecked(&stack) as usize]
            )
            .to_string();

            while !stack.is_empty() && *stack.last().unwrap() == self.get_dim()[stack.len() - 1] - 1
            {
                stack.pop();
                element_str.push(']');
            }

            if !stack.is_empty() {
                *stack.last_mut().unwrap() += 1;
                element_str.push_str(", ");
                while stack.len() < self.no_dim() as usize {
                    stack.push(0);
                    element_str.push('[');
                }
            }
        }

        let res = format!(
            "Tensor ({}, dtype={}, dim={:?}, strides={:?}, offset={})",
            element_str,
            self.get_dtype(),
            self.get_dim(),
            self.get_strides(),
            self.get_offset()
        );
        write!(f, "{}", res)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn contiguous() {
        let t = Tensor::<i32>::arange(0, 27, 1)
            .unwrap()
            .reshape(vec![3, 3, 3])
            .unwrap();
        assert!(t.is_contiguous());

        let ct1 = t.get(vec![0..2, 0..2, 0..2]).unwrap();
        let ct2 = t.get(vec![1..2, 1..2, 1..2]).unwrap();

        assert!(!ct1.is_contiguous());
        assert!(!ct2.is_contiguous());

        let nt1 = ct1.to_contiguous();
        let nt2 = ct2.to_contiguous();
        let nt3 = nt1.get(vec![0..2, 0..2, 1..2]).unwrap().to_contiguous();

        assert!(nt1.is_contiguous());
        assert!(nt2.is_contiguous());
        assert!(nt3.is_contiguous());

        t.get_storage_ptr().borrow_mut().upd_vec(4, 100);
        assert!(!nt1.get_storage_ptr().borrow().get_vec().contains(&100));
        assert!(!nt2.get_storage_ptr().borrow().get_vec().contains(&100));
        assert!(!nt3.get_storage_ptr().borrow().get_vec().contains(&100));
    }
}
