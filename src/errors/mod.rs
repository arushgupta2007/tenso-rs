#[derive(Debug)]
pub enum ErrorKind {
    CannotCastToDtype(String),
    UnknownType,
    InvalidParameter(String),
}
