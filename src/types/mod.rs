use std::fmt;

use super::errors;

#[derive(Clone, Copy, Debug)]
pub enum Types {
    Uint8,
    Uint16,
    Uint32,
    Uint64,
    Uint128,
    Int8,
    Int16,
    Int32,
    Int64,
    Int128,
    Float32,
    Float64,
}

impl fmt::Display for Types {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Types::Uint8 => write!(f, "Uint8"),
            Types::Uint16 => write!(f, "Uint16"),
            Types::Uint32 => write!(f, "Uint32"),
            Types::Uint64 => write!(f, "Uint64"),
            Types::Uint128 => write!(f, "Uint128"),
            Types::Int8 => write!(f, "Int8"),
            Types::Int16 => write!(f, "Int16"),
            Types::Int32 => write!(f, "Int32"),
            Types::Int64 => write!(f, "Int64"),
            Types::Int128 => write!(f, "Int128"),
            Types::Float32 => write!(f, "Float32"),
            Types::Float64 => write!(f, "Float64"),
        }
    }
}

// HACK: Converting primitive types to custom types::Types using their bounds is a hack
pub fn type_from_range(mn: f64, mx: f64) -> Result<Types, errors::ErrorKind> {
    if (mn - u8::MIN as f64).abs() < 1.0 && (mx - u8::MAX as f64).abs() < 1.0 {
        return Ok(Types::Uint8);
    }
    if (mn - u16::MIN as f64).abs() < 1.0 && (mx - u16::MAX as f64).abs() < 1.0 {
        return Ok(Types::Uint16);
    }
    if (mn - u32::MIN as f64).abs() < 1.0 && (mx - u32::MAX as f64).abs() < 1.0 {
        return Ok(Types::Uint32);
    }
    if (mn - u64::MIN as f64).abs() < 1.0 && (mx - u64::MAX as f64).abs() < 1.0 {
        return Ok(Types::Uint64);
    }
    if (mn - u128::MIN as f64).abs() < 1.0 && (mx - u128::MAX as f64).abs() < 1.0 {
        return Ok(Types::Uint128);
    }
    if (mn - i8::MIN as f64).abs() < 1.0 && (mx - i8::MAX as f64).abs() < 1.0 {
        return Ok(Types::Int8);
    }
    if (mn - i16::MIN as f64).abs() < 1.0 && (mx - i16::MAX as f64).abs() < 1.0 {
        return Ok(Types::Int16);
    }
    if (mn - i32::MIN as f64).abs() < 1.0 && (mx - i32::MAX as f64).abs() < 1.0 {
        return Ok(Types::Int32);
    }
    if (mn - i64::MIN as f64).abs() < 1.0 && (mx - i64::MAX as f64).abs() < 1.0 {
        return Ok(Types::Int64);
    }
    if (mn - i128::MIN as f64).abs() < 1.0 && (mx - i128::MAX as f64).abs() < 1.0 {
        return Ok(Types::Int128);
    }
    if (mn - f32::MIN as f64).abs() < 1.0 && (mx - f32::MAX as f64).abs() < 1.0 {
        return Ok(Types::Float32);
    }
    if (mn - f64::MIN).abs() < 1.0 && (mx - f64::MAX).abs() < 1.0 {
        return Ok(Types::Float64);
    }

    Err(errors::ErrorKind::UnknownType)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn get_type_u8() -> Result<(), errors::ErrorKind> {
        match type_from_range(u8::MIN as f64, u8::MAX as f64) {
            Ok(dtype) => match dtype {
                Types::Uint8 => Ok(()),
                _ => Err(errors::ErrorKind::UnknownType),
            },
            Err(e) => Err(e),
        }
    }

    #[test]
    fn get_type_u32() -> Result<(), errors::ErrorKind> {
        match type_from_range(u32::MIN as f64, u32::MAX as f64) {
            Ok(dtype) => match dtype {
                Types::Uint32 => Ok(()),
                _ => Err(errors::ErrorKind::UnknownType),
            },
            Err(e) => Err(e),
        }
    }

    #[test]
    fn get_type_i8() -> Result<(), errors::ErrorKind> {
        match type_from_range(i8::MIN as f64, i8::MAX as f64) {
            Ok(dtype) => match dtype {
                Types::Int8 => Ok(()),
                _ => Err(errors::ErrorKind::UnknownType),
            },
            Err(e) => Err(e),
        }
    }

    #[test]
    fn get_type_i16() -> Result<(), errors::ErrorKind> {
        match type_from_range(i16::MIN as f64, i16::MAX as f64) {
            Ok(dtype) => match dtype {
                Types::Int16 => Ok(()),
                _ => Err(errors::ErrorKind::UnknownType),
            },
            Err(e) => Err(e),
        }
    }

    #[test]
    fn get_type_f32() -> Result<(), errors::ErrorKind> {
        match type_from_range(f32::MIN as f64, f32::MAX as f64) {
            Ok(dtype) => match dtype {
                Types::Float32 => Ok(()),
                _ => Err(errors::ErrorKind::UnknownType),
            },
            Err(e) => Err(e),
        }
    }

    #[test]
    fn get_type_f64() -> Result<(), errors::ErrorKind> {
        match type_from_range(f64::MIN as f64, f64::MAX as f64) {
            Ok(dtype) => match dtype {
                Types::Float64 => Ok(()),
                _ => Err(errors::ErrorKind::UnknownType),
            },
            Err(e) => Err(e),
        }
    }
}
